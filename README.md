# FAU Aberta

## Sobre o projeto

O projeto FAU Aberta (nome provisório) foi criado no segundo semestre de 2019 
para disciplina de Laboratório de Métodos Ágeis, ministrada no Instituto de 
Matemática e Estatística da Universidade de São Paulo.

O intuito do projeto é aumentar a visibilidade das produções realizadas pela 
Faculdade de Arquitetura e Urbanismo da USP, atráves da analise dos dados 
retirados da [plataforma Lattes](http://lattes.cnpq.br/).

Para saber mais sobre o projeto, veja a nossa [wiki](https://gitlab.com/fau-aberta/fau-aberta/wikis/home).
