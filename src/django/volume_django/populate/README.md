# Populando o Banco

## Estrutura:
Entre em: **fau-aberta/src/django/volume_django/populate**  
Neste diretório temos os dados e scripts necessários para popular nosso banco de dados com os dados parseados diretamente do `Currículo Lattes`.

`IMPORTANTE:` Para que o script funcione, é preciso ter acesso aos dados em formato XML, estruturados da seguinte forma:

```
.
├── code
│   ├── DB
│   │   ├── connection.py
│   │   ├── general_operations.py
│   │   └── select_operations.py
│   ├── Parser
│   │   ├── bancas.py
│   │   ├── orientacao.py
│   │   ├── pessoa.py
│   │   ├── premios_titulos.py
│   │   ├── producao_artistica.py
│   │   ├── producao_bibliografica.py
│   │   ├── producao_tecnica.py
│   │   ├── participacao.py
│   └── Sql
│   │   ├── CLEAN_DATABASE.sql
│   │   ├── DATABASE_DDL.sql
│   │   └── DDL_parser.py
│   └── parser.py
├── README.md
├── util
│   └── estrutura
│       ├── estrutura2.txt
│       ├── estrutura3.txt
│       ├── estrutura4.txt
│       ├── estrutura5.txt
│       ├── estrutura6.txt
│       └── estrutura.txt
├── xml
│   ├── DEPARTAMENTO1
│   │   ├── ID_LATTES_1.xml
│   │   ├── ID_LATTES_2.xml
│   │   ├── ID_LATTES_3.xml
│   │   .
│	│	.
│	│	.
│   ├── DEPARTAMENTO2
│   │   ├── ID_LATTES_1.xml
│   │   ├── ID_LATTES_2.xml
│   │   .
│	│	.
│	│	.
│   │
│   └── DEMAIS DEPARTAMENTOS ...
```

### xml/
Possui os arquivos xml com as informações do Currículo Lattes de cada docente, separado em pastas referentes aos departamentos (AUH, AUP, AUT).

### util/
Possui alguns arquivos utilitários
* `estrutura` - arquivos textos que exemplificam a estrutura dos dados do Lattes - foram usados de referência para a construção do parser.
* `sql` - contém scripts em *sql* para gerar as tabelas do banco e limpá-lo, caso necessário. Possui também um parser que automatiza o processo de inserção de tuplas no banco.

### code/
Possui o código em *python* do `parser` dos arquivos xml e de funções usadas para manipular o banco de dados.
* `DB` - pasta que contém funções usadas para se conectar, selecionar e inserir tuplas no banco de dados.
* `Parser` - pasta que contém funções usadas para parsear os diferentes tipos de estrutura de dados encontrados no Lattes, como produção bibliográfica, produção técnica, orientações, etc.
* `parser.py` - script principal que chama as funções definidas acima para popular o banco com todos os arquivos *xml* na pasta xml/.

## Populando

Para popular o banco, execute o seguinte comando:
* `docker exec -it fauserver_django bash -c "cd populate/code; python3 parser.py"`

Caso se queira popular o banco no modo verborrágico, execute o seguinte comando:
* `docker exec -it fauserver_django bash -c "cd populate/code; python3 parser.py -v"`

* `BUG`: Se aparecerem alguns erros de tipos de tabelas, sua imagem docker do banco pode estar desatualizada. Para atualizá-la, é necessário reconstruir o container com o comando:
* `docker-compose build --no-cache`

Para verificar se o banco foi populado, execute os seguintes comandos:
* `docker exec -it fauserver_db psql -U fau_aberta_user -d fau_aberta_db`
* `SELECT * FROM pessoa` - certifique-se de que há pelo menos uma tupla nessa tabela.
