from html import unescape
from bs4 import BeautifulSoup

def parse_orientacao(root, num_identificador):
    orientacoes_array = []

    # MESTRADO
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/ORIENTACOES-CONCLUIDAS-PARA-MESTRADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-MESTRADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-ORIENTACOES-CONCLUIDAS-PARA-MESTRADO")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        orientacoes_dict['ori_tipo_orientacao'] = dados_orientacao.attrib['TIPO'].title()
        orientacoes_dict['ori_tipo_pesquisa'] = "mestrado"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        try:
            orientacoes_dict['ori_flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # DOUTORADO
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        try:
            orientacoes_dict['ori_tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "doutorado"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        try:
            orientacoes_dict['ori_flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # OUTRAS
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/OUTRAS-ORIENTACOES-CONCLUIDAS"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-OUTRAS-ORIENTACOES-CONCLUIDAS")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-OUTRAS-ORIENTACOES-CONCLUIDAS")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        try:
            orientacoes_dict['ori_tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "outras"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        try:
            orientacoes_dict['ori_flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # POS DOUTORADO
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        try:
            orientacoes_dict['ori_tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "pos_doutorado"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        try:
            orientacoes_dict['ori_flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # MESTRADO ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-MESTRADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-MESTRADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-MESTRADO")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        try:
            orientacoes_dict['ori_tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "mestrado_andamento"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # DOUTORADO ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        try:
            orientacoes_dict['ori_tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "doutorado_andamento"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # INICIACAO CIENTIFICA ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-INICIACAO-CIENTIFICA"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-INICIACAO-CIENTIFICA")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-INICIACAO-CIENTIFICA")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "iniciacao_cientifica_andamento"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        try:
            orientacoes_dict['ori_flag'] = ""
        except:
            orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # POS DOUTORADO ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-POS-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-POS-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-POS-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['ori_id'] = ""
        orientacoes_dict['ori_pessoa_id'] = num_identificador
        orientacoes_dict['ori_titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['ori_aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['ori_pais'] = dados_orientacao.attrib['PAIS']
        orientacoes_dict['ori_ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['ori_instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['ori_agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['ori_agencia_fomento'] = ""
        try:
            orientacoes_dict['ori_tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['ori_tipo_orientacao'] = ""
        orientacoes_dict['ori_tipo_pesquisa'] = "pos_doutorado_andamento"

        try:
            orientacoes_dict['ori_natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['ori_natureza'] = ""
        try:
            orientacoes_dict['ori_idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['ori_idioma'] = ""
        try:
            orientacoes_dict['ori_flag'] = ""
        except:
            orientacoes_dict['ori_flag'] = ""
        try:
            orientacoes_dict['ori_doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['ori_doi'] = ""
        try:
            orientacoes_dict['ori_curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['ori_curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    return orientacoes_array
