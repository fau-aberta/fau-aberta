from html import unescape
from bs4 import BeautifulSoup

def parse_bancas(root, num_identificador):

    bancas_array = []

    for bancas in root.findall("DADOS-COMPLEMENTARES/PARTICIPACAO-EM-BANCA-TRABALHOS-CONCLUSAO"):
        for banca in bancas:
            tipo = None
            dados_banca = None
            detalhe_banca = None

            if banca.tag == "OUTRAS-PARTICIPACOES-EM-BANCA":
                dados_banca = banca.find("DADOS-BASICOS-DE-OUTRAS-PARTICIPACOES-EM-BANCA")
                detalhe_banca = banca.find("DETALHAMENTO-DE-OUTRAS-PARTICIPACOES-EM-BANCA")
                tipo = dados_banca.attrib['NATUREZA']
            else:
                tipo = banca.tag[25:]
                dados_banca = banca.find("DADOS-BASICOS-DA-PARTICIPACAO-EM-BANCA-DE-{}".format(tipo))
                detalhe_banca = banca.find("DETALHAMENTO-DA-PARTICIPACAO-EM-BANCA-DE-{}".format(tipo))

            if dados_banca is None:
                print("Exception in parse_bancas: {} with {}".format(num_identificador, banca.tag))

            bancas_dict = {}
            bancas_dict['ban_id'] = ""
            bancas_dict['ban_pessoa_id'] = num_identificador
            bancas_dict['ban_aluno'] = detalhe_banca.attrib['NOME-DO-CANDIDATO']
            bancas_dict['ban_ano'] = dados_banca.attrib['ANO']            
            bancas_dict['ban_titulo'] = BeautifulSoup(unescape(dados_banca.attrib['TITULO']), 'lxml').text
            bancas_dict['ban_pais'] = dados_banca.attrib['PAIS']
            bancas_dict['ban_instituicao'] = detalhe_banca.attrib['NOME-INSTITUICAO']
            bancas_dict['ban_tipo_pesquisa'] = tipo.replace("-", "_").lower().title()

            try:
                bancas_dict['ban_idioma'] = dados_banca.attrib['IDIOMA']
            except:
                bancas_dict['ban_idioma'] = ""
            try:
                bancas_dict['ban_doi'] = dados_banca.attrib['DOI']
            except:
                bancas_dict['ban_doi'] = ""
            try:
                bancas_dict['ban_curso'] = detalhe_banca.attrib['NOME-CURSO'].title()
            except:
                bancas_dict['ban_curso'] = ""

            bancas_array.append(bancas_dict)

    for bancas in root.findall("DADOS-COMPLEMENTARES/PARTICIPACAO-EM-BANCA-JULGADORA"):
        for banca in bancas:

            tipo = ""
            dados_banca = None
            detalhe_banca = None

            if banca.tag == "OUTRAS-BANCAS-JULGADORAS":
                dados_banca = banca.find("DADOS-BASICOS-DE-OUTRAS-BANCAS-JULGADORAS")
                detalhe_banca = banca.find("DETALHAMENTO-DE-OUTRAS-BANCAS-JULGADORAS")
                tipo = dados_banca.attrib["NATUREZA"]
            else:
                tipo = banca.tag[21:]
                dados_banca = banca.find("DADOS-BASICOS-DA-BANCA-JULGADORA-PARA-{}".format(tipo))
                detalhe_banca = banca.find("DETALHAMENTO-DA-BANCA-JULGADORA-PARA-{}".format(tipo))

            if dados_banca is None:
                print("Exception in parse_bancas: {} with {}".format(num_identificador, banca.tag))

            bancas_dict = {}
            bancas_dict['ban_id'] = ""
            bancas_dict['ban_pessoa_id'] = num_identificador
            bancas_dict['ban_aluno'] = ""
            bancas_dict['ban_ano'] = dados_banca.attrib['ANO']
            bancas_dict['ban_titulo'] = BeautifulSoup(unescape(dados_banca.attrib['TITULO']), 'lxml').text
            bancas_dict['ban_pais'] = dados_banca.attrib['PAIS']
            bancas_dict['ban_instituicao'] = detalhe_banca.attrib['NOME-INSTITUICAO']
            bancas_dict['ban_tipo_pesquisa'] = tipo.replace("-", "_").lower().title()

            try:
                bancas_dict['ban_idioma'] = dados_banca.attrib['IDIOMA']
            except:
                bancas_dict['ban_idioma'] = ""
            try:
                bancas_dict['ban_doi'] = dados_banca.attrib['DOI']
            except:
                bancas_dict['ban_doi'] = ""
            try:
                bancas_dict['ban_curso'] = detalhe_banca.attrib['NOME-CURSO'].title()
            except:
                bancas_dict['ban_curso'] = ""

            bancas_array.append(bancas_dict)

    return bancas_array
