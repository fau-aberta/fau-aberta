from html import unescape
from bs4 import BeautifulSoup

def parse_participacao(root, num_identificador):

    participacoes_array = []

    for participacoes in root.findall("DADOS-COMPLEMENTARES/PARTICIPACAO-EM-EVENTOS-CONGRESSOS"):
        for participacao in participacoes:
            tipo = participacao.tag
            artigo = "O"
            dados_participacao = participacao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_participacao is None:
                artigo = "A"
                dados_participacao = participacao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_participacao is None:
                artigo = "E"
                dados_participacao = participacao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_participacao is None:
                print("Exception in parse_participacao: {} with {}".format(num_identificador, participacao.tag))


            detalhamento_participacao = participacao.find("DETALHAMENTO-D{}-{}".format(artigo, tipo))

            if detalhamento_participacao is None:
                print("Exception in parse_participacao: {} don't have DETALHAMENTO".format(num_identificador))
                continue

            participacoes_dict = {}
            participacoes_dict['par_id'] = ""
            participacoes_dict['par_tipo'] = participacao.tag.replace("-", "_").lower().title()
            participacoes_dict['par_natureza'] = dados_participacao.attrib['NATUREZA']
            participacoes_dict['par_titulo'] = BeautifulSoup(unescape(dados_participacao.attrib['TITULO']), 'lxml').text
            participacoes_dict['par_pais'] = dados_participacao.attrib['PAIS']
            participacoes_dict['par_ano'] = dados_participacao.attrib['ANO']
            participacoes_dict['par_tipo_par'] = dados_participacao.attrib['TIPO-PARTICIPACAO'].title()
            participacoes_dict['par_forma_par'] = dados_participacao.attrib['FORMA-PARTICIPACAO']

            try:
                participacoes_dict['par_nome_ev'] = detalhamento_participacao.attrib['NOME-DO-EVENTO']
            except:
                participacoes_dict['par_nome_ev'] = ""
            try:
                participacoes_dict['par_idioma'] = dados_participacao.attrib['IDIOMA']
            except:
                participacoes_dict['par_idioma'] = ""
            try:
                participacoes_dict['par_meio'] = dados_participacao.attrib['MEIO-DE-DIVULGACAO']
            except:
                participacoes_dict['par_meio'] = ""
            try:
                participacoes_dict['par_flag'] = dados_participacao.attrib['FLAG-RELEVANCIA']
            except:
                participacoes_dict['par_flag'] = ""
            try:                
                participacoes_dict['par_doi'] = dados_participacao.attrib['DOI']
            except:
                participacoes_dict['par_doi'] = ""
            try:                
                participacoes_dict['par_nome_inst'] = detalhamento_participacao.attrib['NOME-INSTITUICAO']
            except:
                participacoes_dict['par_nome_inst'] = ""
            autores = []
            for autor in participacao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            participacoes_dict['art_autores'] = autores

            participacoes_array.append(participacoes_dict)

    return participacoes_array
