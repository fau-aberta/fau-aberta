from html import unescape
from bs4 import BeautifulSoup

def parse_producao_artistica(root, num_identificador):

    producoes_array = []

    for producoes in root.findall("OUTRA-PRODUCAO/PRODUCAO-ARTISTICA-CULTURAL"):
        for producao in producoes:
            tipo = producao.tag
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                print("Exception in parse_producao_artistica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['art_id'] = ""
            producoes_dict['art_tipo'] = producao.tag.replace("-", "_").lower().title()
            producoes_dict['art_natureza'] = dados_producao.attrib['NATUREZA']
            producoes_dict['art_titulo'] = BeautifulSoup(unescape(dados_producao.attrib['TITULO']), 'lxml').text
            producoes_dict['art_pais'] = dados_producao.attrib['PAIS']
            producoes_dict['art_ano'] = dados_producao.attrib['ANO']
            try:
                producoes_dict['art_meio'] = dados_producao.attrib['MEIO-DE-DIVULGACAO']
            except:
                producoes_dict['art_meio'] = ""
            try:
                producoes_dict['art_flag'] = dados_producao.attrib['FLAG-RELEVANCIA']
            except:
                producoes_dict['art_flag'] = ""
            try:
                producoes_dict['art_doi'] = dados_producao.attrib['DOI']
            except:
                producoes_dict['art_doi'] = ""
            try:
                producoes_dict['art_idioma'] = dados_producao.attrib['IDIOMA']
            except:
                producoes_dict['art_idioma'] = ""

            detalhe_producao = producao.find("DETALHAMENTO-DE-{}".format(tipo))
            try:
                producoes_dict['art_premiacao'] = detalhe_producao.attrib['PREMIACAO']
            except:
                producoes_dict['art_premiacao'] = ""
            try:
                producoes_dict['art_exposicao'] = detalhe_producao.attrib['EXPOSICAO']
            except:
                producoes_dict['art_exposicao'] = ""

            autores = []
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producoes_dict['art_autores'] = autores

            producoes_array.append(producoes_dict)
            
    return producoes_array
