
def print_function(name, attributes, types, id_attribute, select_attr):
    parameters = ("%s, " * len(attributes))[:-2]
    attr = ["attr[\"{}\"], ".format(i) for i in attributes]
    formated_attr = ""
    for i in attr:
        formated_attr += i
    formated_attr = formated_attr[:-2]

    types = ["<class '{}'>, ".format(i) for i in types]
    formated_attributes_types = ""
    for i in types:
        formated_attributes_types += i
    formated_attributes_types = formated_attributes_types[:-2]

    cond = ["{} = %s and ".format(i) for i in select_attr]
    formated_cond = "SELECT {0} FROM {1} WHERE ".format(id_attribute, name)
    for i in cond:
        formated_cond += i
    formated_cond = formated_cond[:-5]

    attr_param = ["{}, ".format(i) for i in attributes]
    formated_attr_param = ""
    for i in attr_param:
        formated_attr_param += i
    formated_attr_param = formated_attr_param[:-2]

    cond_select = ["attr[\"{}\"], ".format(i) for i in select_attr]
    formated_cond_select = ""
    for i in cond_select:
        formated_cond_select += i
    formated_cond_select = formated_cond_select[:-2]

    print("""
# Makes an select in {0} table
def select_id_{0}(attr, conn = None, cur = None):
    \"\"\"
    Selects in {0}.

    The select_id_{0} function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to select the
    id of the row from the database.

    :param ev: Event dictionary such as the attributes of the {0} table
    :type ev: dict of ({3})
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    \"\"\"
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute(\"{4}\", [{6}])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()""".format(name, parameters, formated_attr, formated_attributes_types, formated_cond, formated_attr_param, formated_cond_select))

    print("""
# Makes an insert in {0} table
def insert_{0}(attr, conn = None, cur = None):
    \"\"\"
    Inserts in {0}.

    The insert_{0} function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the {0} table
    :type ev: dict of ({3})
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    \"\"\"
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute('INSERT INTO {0} ({5}) VALUES ({1})', [{2}])
        conn.commit()
        cur.execute(\"{4}\", [{6}])
        get_id = cur.fetchall()
        for row in get_id:
            return(row[0])

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()""".format(name, parameters, formated_attr, formated_attributes_types, formated_cond, formated_attr_param, formated_cond_select))

def parser():
    ddl_file = open("DATABASE_DDL.sql").read()

    ddl_commands = ddl_file.split(";")

    for command in ddl_commands:
        serial = False
        if not "CREATE TABLE" in command:
            continue
        command_part = command.split("(")
        table_name = command_part[0].split("CREATE TABLE ")[1]

        table_attributes_name = []
        table_attributes_type = []
        select_attr = []
        id_attribute = ""
        attributes = command_part[1].split("\n")
        i = 0
        for attribute in attributes:
            i += 1
            if "CONSTRAINT" in attribute or "REFERENCES" in attribute:
                break

            if i == 1:
                attribute_prop = attribute.strip().split(" ")
                id_attribute = attribute_prop[0]
                if id_attribute == "":
                    i -= 1
                if "SERIAL" in attribute:
                    serial = True
                    continue

            attribute_prop = attribute.strip().split(" ")
            attribute_name = attribute_prop[0]
            if attribute_name == "":
                continue

            j = 1
            while attribute_prop[j] == "":
                j += 1

            if attribute_prop[j][-1] == ",":
                attribute_type = attribute_prop[j][:-1]
            else:
                attribute_type = attribute_prop[j]

            table_attributes_name.append(attribute_name)
            table_attributes_type.append(attribute_type)

        pk = False
        uni = False
        for attribute in command_part[2:]:
            if not pk:
                if not serial:
                    attr = attribute.strip().split(")")[0]
                    select_attr.append(attr)
                if "UNIQUE" in attribute:
                    uni = True
                else:
                    break
                pk = True
                continue
            if uni:
                all_attr = attribute.strip().split(")")[0].replace(" ", "").split(",")
                for attr in all_attr:
                    select_attr.append(attr)
                if "UNIQUE" in attribute:
                    uni = True
                else:
                    break

        print_function(table_name, table_attributes_name, table_attributes_type, id_attribute, select_attr)

def main():
    print("""import psycopg2
from .connection import *""")
    parser()

if __name__ == '__main__':
	main()
