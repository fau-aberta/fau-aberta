DELETE FROM rel_pessoa_bancas;
DELETE FROM rel_pessoa_prod_art;
DELETE FROM rel_pessoa_prod_tec;
DELETE FROM rel_pessoa_prod_bib;
DELETE FROM rel_pessoa_participacao;
DELETE FROM premios_titulos;
DELETE FROM orientacao;
DELETE FROM bancas;
DELETE FROM participacao;
DELETE FROM producao_tecnica;
DELETE FROM producao_bibliografica;
DELETE FROM producao_artistica;
DELETE FROM colaboradores;
DELETE FROM pessoa;


ALTER SEQUENCE premios_titulos_pre_id_seq RESTART WITH 1;
ALTER SEQUENCE orientacao_ori_id_seq RESTART WITH 1;
ALTER SEQUENCE bancas_ban_id_seq RESTART WITH 1;
ALTER SEQUENCE producao_tecnica_tec_id_seq RESTART WITH 1;
ALTER SEQUENCE producao_bibliografica_bib_id_seq RESTART WITH 1;
ALTER SEQUENCE producao_artistica_art_id_seq RESTART WITH 1;
ALTER SEQUENCE colaboradores_co_id_seq RESTART WITH 1;
ALTER SEQUENCE rel_pessoa_bancas_pe_ban_id_seq RESTART WITH 1;
ALTER SEQUENCE rel_pessoa_prod_art_pe_art_id_seq RESTART WITH 1;
ALTER SEQUENCE rel_pessoa_prod_tec_pe_tec_id_seq RESTART WITH 1;
ALTER SEQUENCE rel_pessoa_prod_bib_pe_bib_id_seq RESTART WITH 1;


DROP TABLE rel_pessoa_bancas;
DROP TABLE rel_pessoa_prod_art;
DROP TABLE rel_pessoa_prod_tec;
DROP TABLE rel_pessoa_prod_bib;
DROP TABLE rel_pessoa_participacao;
DROP TABLE premios_titulos;
DROP TABLE orientacao;
DROP TABLE bancas;
DROP TABLE participacao;
DROP TABLE producao_tecnica;
DROP TABLE producao_bibliografica;
DROP TABLE producao_artistica;
DROP TABLE colaboradores;
DROP TABLE pessoa;