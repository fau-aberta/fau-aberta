import psycopg2
from .connection import *

# Makes an select in pessoa table
def select_id_pessoa_by_name(nome, conn = None, cur = None):
    """
    Selects in pessoa.

    The select_id_pessoa function receives a name and select the id of the row
    from the database.

    :param nome: The name of the person
    :type nome: <class 'text'>
    :return id: The id os the selected row
    :rtype: int

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    if conn is None or cur is None:
        conn, cur = get_db()
    try:
        cur.execute("SELECT pe_id_lattes FROM pessoa WHERE pe_nome_completo = %s", [nome])
        get_id = cur.fetchall()
        for row in get_id:
            return(int(row[0]))
        return None
    except (Exception, psycopg2.Error) as error:
        print ("Error while selecting", error)
        conn.rollback()
    cur.close()
    conn.close()
