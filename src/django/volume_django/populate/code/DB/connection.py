import psycopg2

# Dictionary of Data Base properties
db_dict = {
    'database':'fau_aberta_db',
    'user':'fau_aberta_user',
    'password':'p',
    'host':'db',
    'port':'5432',
}

# Connects to the db
def get_db(json=False):
    port = db_dict['port']
    user = db_dict['user']
    database = db_dict['database']
    password = db_dict['password']
    host = db_dict['host']
    if json:
        database = 'json_data'
    try:
        connection = psycopg2.connect(user = user,
                                      password = password,
                                      host = host,
                                      port = port,
                                      database = database)
        cursor = connection.cursor()
        return connection, cursor
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

# Makes a simple query
def make_query(query):
    conn, cur = get_db()
    cur.execute(query)
    return cur.fetchall()
