import json
from django.test import TestCase, Client
from django.urls import reverse

from lattes_data.models import ProducaoArtistica, ProducaoBibliografica, ProducaoTecnica,\
Orientacao, Bancas, PremiosTitulos

class ViewDashboardTests(TestCase):
    """ Test module for dashboard views """
    # fixture file
    fixtures = [
                'util/fixtures/docentes.json',
    			'producao_bibliografica/fixtures/tests.json',
                 'producao_tecnica/fixtures/tests.json', 
                 'producao_artistica/fixtures/tests.json',
                 'orientacao/fixtures/tests.json',
                 'bancas/fixtures/tests.json',
                 'premios/fixtures/tests.json'
            ]

    # initialize the APIClient app
    client = Client()
        
    def test_index_returns_all_elements(self):
        # get API response
        response = self.client.get(reverse('dashboard-index'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        # returns 6 elements: 
        self.assertEqual(len(producao_list), 6)

        types = ['Produção Artística', 
            'Produção Técnica',
            'Produção Bibliográfica',
            'Orientação',
            'Bancas', 
            'Prêmios e Títulos']

        for type in types:
        	self.assertIn(type, producao_list)

    def test_index_returns_correct_count(self):
        # get API response
        response = self.client.get(reverse('dashboard-index'))
        json_response = json.loads(response.content)

        types = ['Produção Artística', 
            'Produção Técnica',
            'Produção Bibliográfica',
            'Orientação',
            'Bancas', 
            'Prêmios e Títulos']

        count = [6, 6, 6, 6, 6, 6]

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        for i in range(6):
        	self.assertEqual(producao_list[types[i]], count[i])

    def test_search_returns_correct_count(self):
        # get API response
        response = self.client.get(reverse('dashboard-search'), {'key': "tulo 1"})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        types = ['prod_art', 
            'prod_bib',
            'prod_tec',
            'bancas',
            'orientacao',
            'premiosTitulos']

        count = [1, 1, 1, 1, 1, 0]

        for i in range(6):
        	self.assertEqual(len(producao_list[types[i]]), count[i])

        