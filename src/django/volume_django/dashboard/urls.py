from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /dashboard/
    url(r'^$', views.index, name='dashboard-index'),
    # ex: /dashboard/search/
    url(r'^search$', views.search, name='dashboard-search'),
    # ex: /dashboard/count/
    url(r'^count$', views.count, name='dashboard-count'),
    # ex: /dashboard/countByDep/
    url(r'^countByDep$', views.countByDep, name='dashboard-countByDep'),
    # ex: /dashboard/countByDepTipo/
    url(r'^countByDepTipo$', views.countByDepTipo, name='dashboard-countByDepTipo'),
    # ex: /dashboard/indexByDep/
    url(r'^indexByDep$', views.indexByDep, name='dashboard-indexByDep'),
    # ex: /dashboard/keywords/
    url(r'^keywords$', views.keywords, name='dashboard-keywords'),
    # ex: /dashboard/map/
    url(r'^map$', views.map, name='dashboard-map'),
]
