from django.db.models import Count
from django.http import JsonResponse
from lattes_data.models import ProducaoArtistica, ProducaoBibliografica, ProducaoTecnica,\
Orientacao, Bancas, PremiosTitulos, Pessoa
import datetime
import pickle

now = datetime.datetime.now()
actual_year = str(now.year)

def index(request):
    """Index for producoes route.

    Parameters: a GET request no parameters.

    Returns:
    JsonResponse with status code and the count of productions for each table.
   """

    dict = {'Produção Artística': [ProducaoArtistica, 'art_id'], 
    'Produção Técnica': [ProducaoTecnica, 'tec_id'],
    'Produção Bibliográfica': [ProducaoBibliografica, 'bib_id'],
    'Orientação': [Orientacao, 'ori_id'],
    'Bancas': [Bancas, 'ban_id'], 
    'Prêmios e Títulos': [PremiosTitulos, 'pre_id']}

    result = {}
    for key, value in dict.items():
        result[key] = value[0].objects.aggregate(Count(value[1]))[value[1]+'__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def indexByDep(request):
    """Index by department for dashboard route.

    Parameters: a GET request with departamento parameter

    Returns:
    JsonResponse with status code and the count of productions for
    each table for the department.
   """
    department = request.GET.get('departamento', '')    
    if not department:
        return error('Departamento inválido')

    author_from_dep = Pessoa.objects.filter(
        pe_departamento__contains=department).values('pe_id_lattes')

    dict = {'Produção Artística': [ProducaoArtistica.objects.filter(
        autores__in=author_from_dep).distinct(), 'art_id'], 
    'Produção Técnica': [ProducaoTecnica.objects.filter(
        autores__in=author_from_dep).distinct(), 'tec_id'],
    'Produção Bibliográfica': [ProducaoBibliografica.objects.filter(
        autores__in=author_from_dep).distinct(), 'bib_id'],
    'Orientação': [Orientacao.objects.filter(
        ori_pessoa__in=author_from_dep).distinct(), 'ori_id'],
    'Bancas': [Bancas.objects.filter(
        participantes__in=author_from_dep).distinct(), 'ban_id'], 
    'Prêmios e Títulos': [PremiosTitulos.objects.filter(
        pre_pe__in=author_from_dep).distinct(), 'pre_id']}

    result = {}
    for key, value in dict.items():
        result[key] = value[0].aggregate(Count(value[1]))[value[1]+'__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def error(msg):
    response_data = {
        'code': 404,
        'result': msg,
    }
    return JsonResponse(response_data)

def search(request):
    """Search for dashboard route.

    Parameters: a GET request with 'key' parameter.

    Returns:
    JsonResponse with all registries that contains the given key
    in its title.
    """
    
    key = request.GET.get('key', '')

    if not key:
        return error("Palavra chave para busca não informada")
    
    result = {
        "prod_art": [],
        "prod_bib": [],
        "prod_tec": [],
        "bancas": [],
        "orientacao": [],
        "premiosTitulos": []
    }

    prod_art = ProducaoArtistica.objects.filter(
        art_titulo__icontains=key).order_by('-art_ano').distinct()    
    for art in prod_art.all():
        obj = {
            "titulo": art.art_titulo,
            "tipo": art.art_tipo,
            "ano": art.art_ano,
            "autores": []
        }
        for autor in art.autores.all():
            idLattes = str(autor.pe_id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": autor.pe_nome_completo,
                "id_lattes": idLattes,
                "departamento": autor.pe_departamento
            })
        result["prod_art"].append(obj)

    prod_bib = ProducaoBibliografica.objects.filter(
        bib_titulo__icontains=key).order_by('-bib_ano').distinct()
    for bib in prod_bib.all():
        obj = {
            "titulo": bib.bib_titulo,
            "tipo": bib.bib_tipo,
            "ano": bib.bib_ano,
            "autores": []
        }
        for autor in bib.autores.all():
            idLattes = str(autor.pe_id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": autor.pe_nome_completo,
                "id_lattes": idLattes,
                "departamento": autor.pe_departamento
            })
        result["prod_bib"].append(obj)

    prod_tec = ProducaoTecnica.objects.filter(
        tec_titulo__icontains=key).order_by('-tec_ano').distinct()
    for tec in prod_tec.all():
        obj = {
            "titulo": tec.tec_titulo,
            "tipo": tec.tec_tipo,
            "ano": tec.tec_ano,
            "autores": []
        }
        for autor in tec.autores.all():
            idLattes = str(autor.pe_id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": autor.pe_nome_completo,
                "id_lattes": idLattes,
                "departamento": autor.pe_departamento
            })
        result["prod_tec"].append(obj)
    
    bancas = Bancas.objects.filter(
        ban_titulo__icontains=key).order_by('-ban_ano').distinct()
    for ban in bancas.all():
        obj = {
            "titulo": ban.ban_titulo,
            "tipo": ban.ban_tipo_pesquisa,
            "ano": ban.ban_ano,
            "autores": []
        }
        for part in ban.participantes.all():
            idLattes = str(autor.pe_id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": part.pe_nome_completo,
                "id_lattes": idLattes,
                "departamento": part.pe_departamento
            })
        result["bancas"].append(obj)
    
    orientacao = Orientacao.objects.filter(
        ori_titulo__icontains=key).order_by('-ori_ano').distinct()
    for ori in orientacao.all():
        idLattes = str(ori.ori_pessoa.pe_id_lattes)
        while len(idLattes) < 16:
            idLattes = str(0) + idLattes
        obj = {
            "titulo":ori.ori_titulo,
            "tipo":ori.ori_tipo_orientacao,
            "ano": ori.ori_ano,
            "autorId": idLattes,
            "autorNome": ori.ori_pessoa.pe_nome_completo,
            "autorDep": ori.ori_pessoa.pe_departamento
        }
        result["orientacao"].append(obj)

    premiosTitulos = PremiosTitulos.objects.filter(
        pre_nome__icontains=key).order_by('-pre_ano').distinct()
    for pre in premiosTitulos.all():
        idLattes = str(pre.pre_pe.pe_id_lattes)
        while len(idLattes) < 16:
            idLattes = str(0) + idLattes
        obj = {
            "nome": pre.pre_nome,
            "ano": pre.pre_ano,
            "autorId": idLattes,
            "autorNome": pre.pre_pe.pe_nome_completo,
            "autorDep": pre.pre_pe.pe_departamento
        }
        result["premiosTitulos"].append(obj)

    response_data = {
        "code": 200,
        "result": result
    }
    
    return JsonResponse(response_data)

def count(request):
    """Count for dashboard route.

    Parameters: a GET request with optionals 'ano_inicio' and 'ano_fim'
    parameters.
    Returns:
    JsonResponse with status code and the count of all productions.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')
    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]
    result = {'FAU': {}}
    prodsArt = ProducaoArtistica.objects.all().distinct()
    for l in labels:
        count = prodsArt.filter(art_ano=l).aggregate(Count('art_id'))
        result['FAU'][l] = count['art_id__count']
    prodsTec = ProducaoTecnica.objects.all().distinct()
    for l in labels:
        count = prodsTec.filter(tec_ano=l).aggregate(Count('tec_id'))
        result['FAU'][l] += count['tec_id__count']
    prodsBib = ProducaoBibliografica.objects.all().distinct()
    for l in labels:
        count = prodsBib.filter(bib_ano=l).aggregate(Count('bib_id'))
        result['FAU'][l] += count['bib_id__count']
    prodsBancas = Bancas.objects.all().distinct()
    for l in labels:
        count = prodsBancas.filter(ban_ano=l).aggregate(Count('ban_id'))
        result['FAU'][l] += count['ban_id__count']
    prodsOri = Orientacao.objects.all().distinct()
    for l in labels:
        count = prodsOri.filter(ori_ano=l).aggregate(Count('ori_id'))
        result['FAU'][l] += count['ori_id__count']
    prodsPremios = PremiosTitulos.objects.all().distinct()
    for l in labels:
        count = prodsPremios.filter(pre_ano=l).aggregate(Count('pre_id'))
        result['FAU'][l] += count['pre_id__count']
    
    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def countByDep(request):
    """Count by department for dashboard route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim'
    and 'departamento' parameters.
    Returns:
    JsonResponse with status code and the count of all productions
    for the given department.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    department = request.GET.get('departamento', '')    
    if not department:
        return error('Departamento inválido')
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')
    
    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]
    result = {department: {}}

    author_from_dep = Pessoa.objects.filter(
        pe_departamento__contains=department).values('pe_id_lattes')

    prodsArt = ProducaoArtistica.objects.filter(
        autores__in=author_from_dep).distinct()
    for l in labels:
        count = prodsArt.filter(art_ano=l).aggregate(Count('art_id'))
        result[department][l] = count['art_id__count']
    prodsTec = ProducaoTecnica.objects.filter(
        autores__in=author_from_dep).distinct()
    for l in labels:
        count = prodsTec.filter(tec_ano=l).aggregate(Count('tec_id'))
        result[department][l] += count['tec_id__count']
    prodsBib = ProducaoBibliografica.objects.filter(
        autores__in=author_from_dep).distinct()
    for l in labels:
        count = prodsBib.filter(bib_ano=l).aggregate(Count('bib_id'))
        result[department][l] += count['bib_id__count']
    prodsBancas = Bancas.objects.filter(
        participantes__in=author_from_dep).distinct()
    for l in labels:
        count = prodsBancas.filter(ban_ano=l).aggregate(Count('ban_id'))
        result[department][l] += count['ban_id__count']
    prodsOri = Orientacao.objects.filter(
        ori_pessoa__in=author_from_dep).distinct()
    for l in labels:
        count = prodsOri.filter(ori_ano=l).aggregate(Count('ori_id'))
        result[department][l] += count['ori_id__count']
    prodsPremios = PremiosTitulos.objects.filter(
        pre_pe__in=author_from_dep).distinct()
    for l in labels:
        count = prodsPremios.filter(pre_ano=l).aggregate(Count('pre_id'))
        result[department][l] += count['pre_id__count']
    
    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def countByDepTipo(request):
    """Count by department and table for dashboard route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamento' and 'tipo' parameters.
    Returns:
    JsonResponse with status code and the count of all productions
    for the given department and given table.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    department = request.GET.get('departamento', '')    
    if not department:
        return error('Departamento inválido')
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')
    table = request.GET.get('tipo', '')
    if not table:
        return error('Tipo inválido')
    
    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]
    result = {department: {}}

    author_from_dep = Pessoa.objects.filter(
        pe_departamento__contains=department).values('pe_id_lattes')

    if table == 'Produção Artística':
        prodsArt = ProducaoArtistica.objects.filter(
            autores__in=author_from_dep).distinct()
        for l in labels:
            count = prodsArt.filter(art_ano=l).aggregate(Count('art_id'))
            result[department][l] = count['art_id__count']
    if table == 'Produção Técnica':
        prodsTec = ProducaoTecnica.objects.filter(
            autores__in=author_from_dep).distinct()
        for l in labels:
            count = prodsTec.filter(tec_ano=l).aggregate(Count('tec_id'))
            result[department][l] = count['tec_id__count']
    if table == 'Produção Bibliográfica':
        prodsBib = ProducaoBibliografica.objects.filter(
            autores__in=author_from_dep).distinct()
        for l in labels:
            count = prodsBib.filter(bib_ano=l).aggregate(Count('bib_id'))
            result[department][l] = count['bib_id__count']
    if table == 'Bancas':
        prodsBancas = Bancas.objects.filter(
            participantes__in=author_from_dep).distinct()
        for l in labels:
            count = prodsBancas.filter(ban_ano=l).aggregate(Count('ban_id'))
            result[department][l] = count['ban_id__count']
    if table == 'Orientação':
        prodsOri = Orientacao.objects.filter(
            ori_pessoa__in=author_from_dep).distinct()
        for l in labels:
            count = prodsOri.filter(ori_ano=l).aggregate(Count('ori_id'))
            result[department][l] = count['ori_id__count']
    if table == 'Prêmios e Títulos':
        prodsPremios = PremiosTitulos.objects.filter(
            pre_pe__in=author_from_dep).distinct()
        for l in labels:
            count = prodsPremios.filter(pre_ano=l).aggregate(Count('pre_id'))
            result[department][l] = count['pre_id__count']
    
    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def keywords(request):
    """Keywords for dashboard route.

    Parameters: a GET request with departamento and
    optional 'limit' parameter.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
    """

    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido.')
    if not limit > 0:
        return error('Limite inválido.')
    department = request.GET.get('departamento', '')    
    if not department:
        return error('Departamento inválido')

    result = []

    keywordsProdArt = ProducaoArtistica.keywordsDep(limit, department)
    keywordsProdBib = ProducaoBibliografica.keywordsDep(limit, department)
    keywordsProdTec = ProducaoTecnica.keywordsDep(limit, department)
    keywordsOri = Orientacao.keywordsDep(limit, department)
    keywordsBancas = Bancas.keywordsDep(limit, department)

    keyList = [keywordsProdArt, keywordsProdBib, keywordsProdTec, keywordsOri, keywordsBancas]
    
    keywords = {}

    for keys in keyList:
        for keyword in keys:
            key = keyword[0]
            if not key in keywords:
                keywords[key] = keyword[1]
            else:
                keywords[key] += keyword[1]
               
    for keyword in keywords:
        result.append({keyword: keywords[keyword]})
        
    return JsonResponse({'code': 200, 'result': result[:limit]})


def map(request):
    """List of countries for dashboard route.

    Parameters: a GET request with departamento and
    optionals 'limit' parameters.

    Returns:
    JsonResponse with status code and the list of countries and the specific 
    orientation counter.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido.')
    if not limit > 0:
        return error('Limite inválido.')
    department = request.GET.get('departamento', '')    
    if not department:
        return error('Departamento inválido')

    author_from_dep = Pessoa.objects.filter(
        pe_departamento__contains=department).values('pe_id_lattes')

    countriesProdArt = ProducaoArtistica.objects.filter(
        autores__in=author_from_dep).distinct().values(
            'art_pais').annotate(total=Count('art_pais')).order_by('-total')[:limit]
    countriesProdBib = ProducaoBibliografica.objects.filter(
        autores__in=author_from_dep).distinct().values(
            'bib_pais').annotate(total=Count('bib_pais')).order_by('-total')[:limit]
    countriesProdTec = ProducaoTecnica.objects.filter(
        autores__in=author_from_dep).distinct().values(
            'tec_pais').annotate(total=Count('tec_pais')).order_by('-total')[:limit]
    countriesBancas = Bancas.objects.filter(
        participantes__in=author_from_dep).distinct().values(
            'ban_pais').annotate(total=Count('ban_pais')).order_by('-total')[:limit]
    countriesOri = Orientacao.objects.filter(
        ori_pessoa__in=author_from_dep).distinct().values(
            'ori_pais').annotate(total=Count('ori_pais')).order_by('-total')[:limit]
    result = []

    with open('util/countries.pickle', 'rb') as file:
        country_dict = pickle.load(file)

    countryList = {
        'obj': [countriesProdArt, countriesProdBib, countriesProdTec,
        countriesBancas, countriesOri],
        'prefix': ['art', 'bib', 'tec', 'ban', 'ori']
    }

    countries = {}
    for i, obj in enumerate(countryList['obj']):
        for country in obj:
            try:
                geolocation = country_dict[country[countryList['prefix'][i] + '_pais']]
                if not country[countryList['prefix'][i] + '_pais'] in countries:
                    countries[country[countryList['prefix'][i] + '_pais']] = {'lat': geolocation['lat'],
                        'long': geolocation['long'], 'total': country['total']}
                else:
                    countries[country[countryList['prefix'][i] + '_pais']]['total'] += country['total']
            except:
                pass
    
    for geo in countries:
        result.append({geo: countries[geo]})

    return JsonResponse({'code': 200, 'result': result})
