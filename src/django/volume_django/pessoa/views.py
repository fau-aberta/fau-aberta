from django.db.models import Count, Q
from django.http import JsonResponse
from util.format import make_chart_js
from lattes_data.models import Pessoa
import pickle
import datetime
import re

now = datetime.datetime.now()
actual_year = str(now.year)

def error(msg):
    response_data = {
        'code': 404,
        'result': msg,
    }
    return JsonResponse(response_data)

def index(request):
    """Index for pessoa route.

    Parameters: a GET request with 'idLattes' parameter.

    Returns:
    JsonResponse with status code and all the information of the person
    with the given id.
    """

    id_lattes = request.GET.get('id', '')

    if not id_lattes.isdigit():
        return error('Id inválido')

    id_lattesInt = int(id_lattes)
    query_set = Pessoa.objects.filter(
        pe_id_lattes = id_lattesInt)
    if not query_set:
        return error('Id inválido')
    result = {
        "id_lattes": id_lattes,
        "nome_completo": query_set[0].pe_nome_completo,
        "nome_citacao": query_set[0].pe_nome_citacao,
        "nacionalidade": query_set[0].pe_nacionalidade,
        "contato": query_set[0].pe_contato,
        "departamento": query_set[0].pe_departamento,
        "cidade": query_set[0].pe_cidade,
        "estado": query_set[0].pe_estado,
        "resumo": query_set[0].pe_resumo,
        "producoes_bibliograficas": [],
        "producoes_artisticas": [],
        "producoes_tecnicas": [],
        "bancas": [],
        "orientacoes": [],
        "premios_titulos": [],
        "participacoes": []
    }
    for prod_bib in query_set[0].producoes_bibliograficas.all():
        autores = []
        for autor in prod_bib.bib_autores.split(","):
            autores.append(re.sub(r'[\"\{\}]', '', autor))
        result["producoes_bibliograficas"].append({
            "id": prod_bib.bib_id,
            "tipo": prod_bib.bib_tipo.replace('_', ' '),
            "titulo": prod_bib.bib_titulo.replace('_', ' '),
            "ano": prod_bib.bib_ano,
            "autores": autores,
            "publicacao": prod_bib.bib_publicacao,
            "editora": prod_bib.bib_editora,
            "edicao": prod_bib.bib_edicao,
            "volume": prod_bib.bib_volume,
            "natureza": prod_bib.bib_natureza,
            "pais": prod_bib.bib_pais,
            "idioma": prod_bib.bib_idioma,
            "meio": prod_bib.bib_meio,
            "flag": prod_bib.bib_flag,
            "doi": prod_bib.bib_doi,
            "isbn": prod_bib.bib_isbn
        })
    for prod_art in query_set[0].producoes_artisticas.all():
        autores = []
        for autor in prod_art.art_autores.split(","):
            autores.append(re.sub(r'[\"\{\}]', '', autor))
        result["producoes_artisticas"].append({
            "id": prod_art.art_id,
            "tipo": prod_art.art_tipo.replace('_', ' '),
            "titulo": prod_art.art_titulo.replace('_', ' '),
            "ano": prod_art.art_ano,
            "autores": autores,
            "natureza": prod_art.art_natureza,
            "pais": prod_art.art_pais,
            "meio": prod_art.art_meio,
            "flag": prod_art.art_flag,
            "doi": prod_art.art_doi,
            "idioma": prod_art.art_idioma,
            "premiacao": prod_art.art_premiacao,
            "exposicao": prod_art.art_exposicao
        })
    for prod_tec in query_set[0].producoes_tecnicas.all():
        autores = []
        for autor in prod_tec.tec_autores.split(","):
            autores.append(re.sub(r'[\"\{\}]', '', autor))
        result["producoes_tecnicas"].append({
            "id": prod_tec.tec_id,
            "tipo": prod_tec.tec_tipo.replace('_', ' '),
            "titulo": prod_tec.tec_titulo.replace('_', ' '),
            "ano": prod_tec.tec_ano,
            "autores": autores,
            "natureza": prod_tec.tec_natureza,
            "pais": prod_tec.tec_pais,
            "idioma": prod_tec.tec_idioma,
            "meio": prod_tec.tec_meio,
            "flag": prod_tec.tec_flag,
            "finalidade": prod_tec.tec_finalidade,
            "pags": prod_tec.tec_pags
        })
    for banca in query_set[0].aparicoes_bancas.all():
        result["bancas"].append({
            "id": banca.ban_id,
            "aluno": banca.ban_aluno,
            "ano": banca.ban_ano,
            "titulo": banca.ban_titulo.replace('_', ' '),
            "pais": banca.ban_pais,
            "instituicao": banca.ban_instituicao,
            "tipo": banca.ban_tipo_pesquisa.replace('_', ' '),
            "idioma": banca.ban_idioma,
            "doi": banca.ban_doi,
            "curso": banca.ban_curso
        })
    for ori in query_set[0].orientacao_set.all():
        result["orientacoes"].append({
            "id": ori.ori_id,
            "aluno": ori.ori_aluno,
            "ano": ori.ori_ano,
            "titulo": ori.ori_titulo.replace('_', ' '),
            "pais": ori.ori_pais,
            "instituicao": ori.ori_instituicao,
            "agencia_fomento": ori.ori_agencia_fomento,
            "tipo_orientacao": ori.ori_tipo_orientacao.replace('_', ' '),
            "tipo_pesquisa": ori.ori_tipo_pesquisa.replace('_', ' ').title(),
            "natureza": ori.ori_natureza,
            "idioma": ori.ori_idioma,
            "flag": ori.ori_flag,
            "doi": ori.ori_doi,
            "curso": ori.ori_curso
        })
    for pre_tit in query_set[0].premiostitulos_set.all():
        result["premios_titulos"].append({
            "id": pre_tit.pre_id,
            "nome": pre_tit.pre_nome.replace('_', ' '),
            "ano": pre_tit.pre_ano,
            "entidade": pre_tit.pre_entidade
        })
    for part in query_set[0].participacoes.all():
        result["participacoes"].append({
            "id": part.par_id,
            "nome_ev": part.par_nome_ev.replace('_', ' '),
            "tipo": part.par_tipo.replace('_', ' '),
            "ano": part.par_ano,
            "natureza": part.par_natureza,
            "pais": part.par_pais,
            "idioma": part.par_idioma,
            "meio": part.par_meio,
            "flag": part.par_flag,
            "doi": part.par_doi,
            "nome_inst": part.par_nome_inst
        })
    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)

def names(request):
    """Names for pessoa route.

    Parameters: a GET request.

    Returns:
    JsonResponse with status code and all names, idLattes
    and department of people on the database.
    """

    query_set = Pessoa.objects.all()
    if not query_set:
        return error('Não há pessoas registradas')
    result = []
    for pessoa in query_set.order_by('pe_nome_completo'):
        idLattes = str(pessoa.pe_id_lattes)
        while len(idLattes) < 16:
            idLattes = str(0) + idLattes

        result.append({
            "id": idLattes,
            "nome": pessoa.pe_nome_completo,
            "departamento": pessoa.pe_departamento
        })
    response_data = {
        "code": 200,
        "result": result
    }
    
    return JsonResponse(response_data)

def countByDep(request):
    """countByDep for pessoa route.

    Parameters: a GET request with 'departmento' parameter.

    Returns:
    JsonResponse with status code and a count of people
    in the given department.
    """

    department = request.GET.get('departamento', '')
    if not department in ['AUH', 'AUP', 'AUT']:
        return error('Departamento inexistente')

    query_set = Pessoa.objects.filter(
        pe_departamento = department).count()
    
    response_data = {
        "code": 200,
        "result": query_set
    }
    
    return JsonResponse(response_data)

def keywords(request):
    """Keywords for pessoa route.

    Parameters: a GET request with 'idLattes' and
    optional 'limit' parameter.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
    """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido.')

    if not limit > 0:
        return error('Limite inválido.')

    idLattes = request.GET.get('id', '')
    keywords = Pessoa.keywords(idLattes, limit)
    if keywords == 'erro':
        return error('Id inválido.')
    result = []

    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})

    return JsonResponse({'code': 200, 'result': result})

def map(request):
    """Map for producao_artistica route.

    Parameters: a GET request with 'idLattes' and 
    optional 'limit' parameter.

    Returns:
    JsonResponse with status code and the list of countries
    and the specific artistic productions counter.
    """

    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido.')

    if not limit > 0:
        return error('Limite inválido.')

    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    result = []

    with open('util/countries.pickle', 'rb') as file:
        country_dict = pickle.load(file)

    countries_prod_art = query_set[0].producoes_artisticas.all().values(
        'art_pais').annotate(total=Count('art_pais')).order_by('-total')[:limit]
    countries = {}

    for country in countries_prod_art:
        if country['art_pais'] in countries: 
            countries[country['art_pais']] += country['total']
        else:
            countries[country['art_pais']] = country['total']
    
    countries_prod_tec = query_set[0].producoes_tecnicas.all().values(
        'tec_pais').annotate(total=Count('tec_pais')).order_by('-total')[:limit]

    for country in countries_prod_tec:
        if country['tec_pais'] in countries: 
            countries[country['tec_pais']] += country['total']
        else:
            countries[country['tec_pais']] = country['total']

    countries_ban = query_set[0].bancas.all().values(
        'ban_pais').annotate(total=Count('ban_pais')).order_by('-total')[:limit]

    for country in countries_ban:
        if country['ban_pais'] in countries: 
            countries[country['ban_pais']] += country['total']
        else:
            countries[country['ban_pais']] = country['total']

    countries_part = query_set[0].participacoes.all().values(
        'par_pais').annotate(total=Count('par_pais')).order_by('-total')[:limit]
        
    for country in countries_part:
        if country['par_pais'] in countries: 
            countries[country['par_pais']] += country['total']
        else:
            countries[country['par_pais']] = country['total']
    
    countries_ori = query_set[0].orientacao_set.all().values(
        'ori_pais').annotate(total=Count('ori_pais')).order_by('-total')[:limit]

    for country in countries_ori:
        if country['ori_pais'] in countries: 
            countries[country['ori_pais']] += country['total']
        else:
            countries[country['ori_pais']] = country['total']

    for key in countries:
        try:
            geolocation = country_dict[key]
            result.append({key: {'lat': geolocation['lat'],
                'long': geolocation['long'], 'total': countries[key]}})
        except:
            pass
    
    return JsonResponse({'code': 200, 'result': result})

def tiposProdArt(request):
    """Tipos for producao_artistica of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of artistic
    production's types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    types = query_set[0].producoes_artisticas.all().values(
        'art_tipo').distinct().order_by('art_tipo')
    result = []
    accepted = ['Outra_Producao_Artistica_Cultural', 'Artes_Visuais', 'Musica']
    for t in types:
        if t['art_tipo'] in accepted:
            result.append(t['art_tipo'])
    return JsonResponse({'code': 200, 'result': result})


def countTipos(request):
    """Count for pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {'DOCENTE': {}}

    prodsArt = query_set[0].producoes_artisticas.all()
    for l in labels:
        count = prodsArt.filter(art_ano=l).aggregate(Count('art_id'))
        result['DOCENTE'][l] = count['art_id__count']
    
    prodsTec = query_set[0].producoes_tecnicas.all()
    for l in labels:
        count = prodsTec.filter(tec_ano=l).aggregate(Count('tec_id'))
        result['DOCENTE'][l] += count['tec_id__count']

    prodsBib = query_set[0].producoes_bibliograficas.all()
    for l in labels:
        count = prodsBib.filter(bib_ano=l).aggregate(Count('bib_id'))
        result['DOCENTE'][l] += count['bib_id__count']
    
    prodsBan = query_set[0].aparicoes_bancas.all()
    for l in labels:
        count = prodsBan.filter(ban_ano=l).aggregate(Count('ban_id'))
        result['DOCENTE'][l] += count['ban_id__count']

    prodsOri = query_set[0].orientacao_set.all()
    for l in labels:
        count = prodsOri.filter(ori_ano=l).aggregate(Count('ori_id'))
        result['DOCENTE'][l] += count['ori_id__count']
    
    prodsPre = query_set[0].premiostitulos_set.all()
    for l in labels:
        count = prodsPre.filter(pre_ano=l).aggregate(Count('pre_id'))
        result['DOCENTE'][l] += count['pre_id__count']

    return JsonResponse({'code': 200, 'result': result})

def countTiposProdArt(request):
    """Count_tipos for producao_artistica of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    artistic production's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    query = query_set[0].producoes_artisticas.all().values(
        'art_tipo').distinct().order_by('art_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['art_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    result = {}

    for t in requested_types:
        prods = query_set[0].producoes_artisticas.all().filter(
            art_tipo=t).count()
        result[t] = prods

    return JsonResponse({'code': 200, 'result': result})    


def tiposProdTec(request):
    """Tipos for producao_tecnica of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of technical
    production's types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    types = query_set[0].producoes_tecnicas.all().values(
        'tec_tipo').distinct().order_by('tec_tipo')
    result = []
    accepted = ['Trabalho_Tecnico', 'Organizacao_De_Evento',
        'Curso_De_Curta_Duracao_Ministrado', 'Programa_De_Radio_Ou_Tv', 'Apresentacao_De_Trabalho']
    for t in types:
        if t['tec_tipo'] in accepted:
            result.append(t['tec_tipo'])
    return JsonResponse({'code': 200, 'result': result})


def countTiposProdTec(request):
    """Count_tipos for producao_tecnica of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    technical production's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    query = query_set[0].producoes_tecnicas.all().values(
        'tec_tipo').distinct().order_by('tec_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tec_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    result = {}

    for t in requested_types:
        prods = query_set[0].producoes_tecnicas.all().filter(
            tec_tipo=t).count()
        result[t] = prods

    return JsonResponse({'code': 200, 'result': result})


def tiposProdBib(request):
    """Tipos for producao_bibliografica of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of bibliographic
    production's types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    types = query_set[0].producoes_bibliograficas.all().values(
        'bib_tipo').distinct().order_by('bib_tipo')
    result = []
    accepted = ['Livro', 'Capitulo', 'Artigo', 'Evento', 'Jornal', 'Prefacio_Posfacio']
    for t in types:
        if t['bib_tipo'] in accepted:
            result.append(t['bib_tipo'])
    return JsonResponse({'code': 200, 'result': result})


def countTiposProdBib(request):
    """Count_tipos for producao_bibliografica of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    bibliographic production's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    query = query_set[0].producoes_bibliograficas.all().values(
        'bib_tipo').distinct().order_by('bib_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['bib_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    result = {}

    for t in requested_types:
        prods = query_set[0].producoes_bibliograficas.all().filter(
            bib_tipo=t).count()
        result[t] = prods

    return JsonResponse({'code': 200, 'result': result})


def tiposPart(request):
    """Tipos for participacao of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of participations
    types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    types = query_set[0].participacoes.all().values(
        'par_tipo').distinct().order_by('par_tipo')
    result = []
    for t in types:
        result.append(t['par_tipo'])
    return JsonResponse({'code': 200, 'result': result})


def countTiposPart(request):
    """Count_tipos for participacao of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    participation's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    query = query_set[0].participacoes.all().values(
        'par_tipo').distinct().order_by('par_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['par_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    result = {}

    for t in requested_types:
        prods = query_set[0].participacoes.all().filter(
            par_tipo=t).count()
        result[t] = prods

    return JsonResponse({'code': 200, 'result': result})


def tiposBanca(request):
    """Tipos for bancas of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of bancas
    types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    types = query_set[0].aparicoes_bancas.all().values(
        'ban_tipo_pesquisa').distinct().order_by('ban_tipo_pesquisa')
    result = []
    accepted = ['Doutorado', 'Graduacao', 'Livre_Docencia', 'Mestrado', 'Outra']
    for t in types:
        if t['ban_tipo_pesquisa'] in accepted:
            result.append(t['ban_tipo_pesquisa'])
    return JsonResponse({'code': 200, 'result': result})


def countTiposBanca(request):
    """Count_tipos for bancas of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    banca's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    query = query_set[0].aparicoes_bancas.all().values(
        'ban_tipo_pesquisa').distinct().order_by('ban_tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['ban_tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    result = {}

    for t in requested_types:
        prods = query_set[0].aparicoes_bancas.all().filter(
            ban_tipo_pesquisa=t).count()
        result[t] = prods

    return JsonResponse({'code': 200, 'result': result})


def tiposOri(request):
    """Tipos for orientacoes of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of orientacoes
    types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    types = query_set[0].orientacao_set.all().values(
        'ori_tipo_pesquisa').distinct().order_by('ori_tipo_pesquisa')
    result = []
    for t in types:
        result.append(t['ori_tipo_pesquisa'])
    return JsonResponse({'code': 200, 'result': result})


def countTiposOri(request):
    """Count_tipos for orientacao of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    orientacao's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    query = query_set[0].orientacao_set.all().values(
        'ori_tipo_pesquisa').distinct().order_by('ori_tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['ori_tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    result = {}

    for t in requested_types:
        prods = query_set[0].orientacao_set.all().filter(
            ori_tipo_pesquisa=t).count()
        result[t] = prods

    return JsonResponse({'code': 200, 'result': result})


def countProdArtByYear(request):
    """Count for producao_artistica of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of artistic productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    query = query_set[0].producoes_artisticas.all().values(
        'art_tipo').distinct().order_by('art_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['art_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for type in requested_types:
        result[type] = {}
        prods = query_set[0].producoes_artisticas.all().filter(
            art_tipo=type).distinct()
        for l in labels:
            count = prods.filter(art_ano=l).aggregate(Count('art_id'))
            result[type][l] = count['art_id__count']
    
    return JsonResponse({'code': 200, 'result': result})


def countProdTecByYear(request):
    """Count for producao_tecnica of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of technical productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    query = query_set[0].producoes_tecnicas.all().values(
        'tec_tipo').distinct().order_by('tec_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tec_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for type in requested_types:
        result[type] = {}
        prods = query_set[0].producoes_tecnicas.all().filter(
            tec_tipo=type).distinct()
        for l in labels:
            count = prods.filter(tec_ano=l).aggregate(Count('tec_id'))
            result[type][l] = count['tec_id__count']
    
    return JsonResponse({'code': 200, 'result': result})


def countProdBibByYear(request):
    """Count for producao_bibliografica of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of bibliographic productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    query = query_set[0].producoes_bibliograficas.all().values(
        'bib_tipo').distinct().order_by('bib_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['bib_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for type in requested_types:
        result[type] = {}
        prods = query_set[0].producoes_bibliograficas.all().filter(
            bib_tipo=type).distinct()
        for l in labels:
            count = prods.filter(bib_ano=l).aggregate(Count('bib_id'))
            result[type][l] = count['bib_id__count']
    
    return JsonResponse({'code': 200, 'result': result})


def countPartByYear(request):
    """Count for participacao of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of participations
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    query = query_set[0].participacoes.all().values(
        'par_tipo').distinct().order_by('par_tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['par_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for type in requested_types:
        result[type] = {}
        prods = query_set[0].participacoes.all().filter(
            par_tipo=type).distinct()
        for l in labels:
            count = prods.filter(par_ano=l).aggregate(Count('par_id'))
            result[type][l] = count['par_id__count']
    
    return JsonResponse({'code': 200, 'result': result})


def countBancaByYear(request):
    """Count for banca of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of bancas
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    query = query_set[0].aparicoes_bancas.all().values(
        'ban_tipo_pesquisa').distinct().order_by('ban_tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['ban_tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for type in requested_types:
        result[type] = {}
        prods = query_set[0].aparicoes_bancas.all().filter(
            ban_tipo_pesquisa=type).distinct()
        for l in labels:
            count = prods.filter(ban_ano=l).aggregate(Count('ban_id'))
            result[type][l] = count['ban_id__count']
    
    return JsonResponse({'code': 200, 'result': result})


def countOriByYear(request):
    """Count for orientacao of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of orientacao
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    query = query_set[0].orientacao_set.all().values(
        'ori_tipo_pesquisa').distinct().order_by('ori_tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['ori_tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for type in requested_types:
        result[type] = {}
        prods = query_set[0].orientacao_set.all().filter(
            ori_tipo_pesquisa=type).distinct()
        for l in labels:
            count = prods.filter(ori_ano=l).aggregate(Count('ori_id'))
            result[type][l] = count['ori_id__count']
    
    return JsonResponse({'code': 200, 'result': result})


def countPremioByYear(request):
    """Count for premios of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim'
    and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of premios
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(pe_id_lattes = idLattes)
    if not query_set:
        return error('Id inválido')
    
    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    query = query_set[0].premiostitulos_set.all()
        
    result = {'premios_titulos': {}}
    for l in labels:
        count = query.filter(pre_ano=l).aggregate(Count('pre_id'))
        result['premios_titulos'][l] = count['pre_id__count']
    
    return JsonResponse({'code': 200, 'result': result})