from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producao-bibliografica/
    url(r'^$', views.index, name='producao_bibliografica-index'),
    # ex: /producao-bibliografica/count
    url(r'^count$', views.count, name='producao_bibliografica-count'),
    # ex: /producao-bibliografica/keywords
    url(r'^keywords$', views.keywords, name='producao_bibliografica-keywords'),
    # ex: / producao-bibliografica/map
    url(r'^map$', views.map, name='producao_bibliografica-map'),
    # ex: /producao_bibliografica/tipos
    url(r'^tipos$', views.tipos, name='producao_bibliografica-tipos'),
    # ex: /producao_bibliografica/count_tipos
    url(r'^count_tipos$', views.count_tipos,
        name='producao_bibliografica-count_tipos'),
    # ex: /producao_bibliografica/rank
    url(r'^rank$', views.rank, name='producao_bibliografica-rank'),
]
