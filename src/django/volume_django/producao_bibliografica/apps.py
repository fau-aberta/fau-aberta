from django.apps import AppConfig

class ProducaoBibliograficaConfig(AppConfig):
    name = 'producao_bibliografica'
