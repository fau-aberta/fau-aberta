from django.core.paginator import Paginator
from django.db.models import Count, Q
from django.http import JsonResponse
from util.format import make_chart_js
import pickle
from lattes_data.models import Pessoa, ProducaoBibliografica
import datetime

now = datetime.datetime.now()
actual_year = str(now.year)

def error(msg):
    response_data = {
        'code': 404,
        'result': msg,
    }
    return JsonResponse(response_data)

def index(request):
    """Index for producao_bibliografica route.

   Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 'page' and
   'page_size' parameters.

    Returns:
    JsonResponse with status code and a list of premios for each year.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    page = request.GET.get('page', '1')
    page_size = request.GET.get('page_size', '15')

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano inválido')

    if not page.isdigit() or not page_size.isdigit():
        return error('Página ou tamanho de página inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    result = []
    query_set = ProducaoBibliografica.objects.filter(
        bib_ano__gte=int(ini_year), bib_ano__lte=int(end_year)).order_by('-bib_ano')
    production_page = Paginator(query_set, page_size)

    for production in production_page.page(page):
        authors = []
        for author in production.autores.all():
            authors.append({
                "nome": author.pe_nome_completo,
                "id_lattes": author.pe_id_lattes,
                "departamento": author.pe_departamento
            })
        result.append({
            "ano": production.bib_ano,
            "titulo": production.bib_titulo,
            "tipo": production.bib_tipo,
            "autores": authors
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)


def count(request):
    """Count for producao_bibliografica route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamentos[]' and 'tipos[]' parameters.

    Returns:
    JsonResponse with status code and the count of bibliographic productions for
    each type, department and year.
   """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['bib_tipo']
                 for typ in ProducaoBibliografica.objects.values('bib_tipo').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for department in departments:
        result[department] = {}
        for type in types:
            result[department][type] = {}
            author_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            prods = ProducaoBibliografica.objects.filter(
                bib_tipo=type, autores__in=author_from_dep).distinct()

            for l in labels:
                count = prods.filter(bib_ano=l).aggregate(Count('bib_id'))
                result[department][type][l] = count['bib_id__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)


def keywords(request):
    """Keywords for producao_bibliografica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the keywords with your specific frequency.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido')

    if not limit > 0:
        return error('Limite inválido')

    keywords = ProducaoBibliografica.keywords(limit)
    result = []

    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})

    return JsonResponse({'code': 200, 'result': result})


def map(request):
    """Map for producao_bibliografica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries and the specific
    bibliographic productions counter.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido')

    if not limit > 0:
        return error('Limite inválido')

    countries = ProducaoBibliografica.objects.all().values(
        'bib_pais').annotate(total=Count('bib_pais')).order_by('-total')[:limit]
    result = []

    with open('util/countries.pickle', 'rb') as file:
        country_dict = pickle.load(file)

    for country in countries:
        try:
            geolocation = country_dict[country['bib_pais']]
            result.append({country['bib_pais']: {'lat': geolocation['lat'],
                'long': geolocation['long'], 'total': country['total']}})
        except:
            pass

    return JsonResponse({'code': 200, 'result': result})


def tipos(request):
    """Tipos for producao_bibliografica.

    Parameters: None

    Returns:
    JsonResponse with status code and a list of bibliographic production's types.
   """
    types = ProducaoBibliografica.objects.all().values(
        'bib_tipo').distinct().order_by('bib_tipo')
    result = []
    accepted = ['Livro', 'Capitulo', 'Artigo', 'Evento', 'Jornal', 'Prefacio_Posfacio']
    for t in types:
        if t['bib_tipo'] in accepted:
            result.append(t['bib_tipo'])
    return JsonResponse({'code': 200, 'result': result})


def count_tipos(request):
    """Count_tipos for producao_bibliografica route.

    Parameters: a GET request with 'tipos[]' and 'departamentos[]'

    Returns:
    JsonResponse with status code and a list with the ammount of each
    bibliographic production's type.
   """
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['bib_tipo']
                 for typ in ProducaoBibliografica.objects.values('bib_tipo').distinct()]

    result = {}
    for department in departments:
        result[department] = []
        for type in types:
            values = {}
            author_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            prods_count = ProducaoBibliografica.objects.filter(
                autores__in=author_from_dep).distinct().filter(bib_tipo=type).count()
            values[type] = prods_count
            result[department].append(values)

    response_data = {
        "code": 200,
        "result": result,
    }
    return JsonResponse(response_data)


def rank(request):
    """Ranking for producao_bibliografica route.

    Parameters: a GET request with optionals ano_inicio', 'ano_fim',
    'departamentos', 'tipos' and 'limit' parameters.

    Returns:
    JsonResponse with status code and a list of the <limit> docentes who created
    the most bibliographic productions and the respective number of creations.
   """
    try:
        limit = int(request.GET.get('limit', 10))
    except ValueError:
        return error('Limite inválido')

    if not limit > 0:
        return error('Limite inválido')

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['bib_tipo']
                 for typ in ProducaoBibliografica.objects.values('bib_tipo').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)

    ranking = Pessoa.objects\
        .filter(Q(pe_departamento__in=departments) &
                (Q(producoes_bibliograficas__bib_tipo__in=types,
                   producoes_bibliograficas__bib_ano__lte=int(end_year),
                   producoes_bibliograficas__bib_ano__gte=int(ini_year)) |
                 Q(producoes_bibliograficas=None)))\
        .annotate(prod_count=Count('producoes_bibliograficas'))\
        .order_by('-prod_count')[:limit]

    result = []

    for value in ranking:
        result.append({
            "nome": value.pe_nome_completo,
            "numero_de_producoes": value.prod_count
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)
