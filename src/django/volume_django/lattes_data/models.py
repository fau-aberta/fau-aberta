from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Bancas(models.Model):
    ban_id = models.AutoField(primary_key=True)
    ban_aluno = models.TextField()
    ban_ano = models.IntegerField()
    ban_titulo = models.TextField()
    ban_pais = models.TextField(blank=True, null=True)
    ban_instituicao = models.TextField(blank=True, null=True)
    ban_tipo_pesquisa = models.TextField(blank=True, null=True)
    ban_idioma = models.TextField(blank=True, null=True)
    ban_doi = models.TextField(blank=True, null=True)
    ban_curso = models.TextField(blank=True, null=True)
    participantes = models.ManyToManyField(
        'Pessoa', through='RelPessoaBancas', related_name='bancas')
    
    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for banca in Bancas.objects.all():
            title = tokenizer.tokenize(banca.ban_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                        stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_words

    @staticmethod
    def keywordsDep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects.filter(
            pe_departamento__contains=dep).values('pe_id_lattes')
        for banca in Bancas.objects.filter(
            participantes__in=author_from_dep).distinct():
            title = tokenizer.tokenize(banca.ban_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                        stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_words
    
    class Meta:
        managed = False
        db_table = 'bancas'
        unique_together = (('ban_aluno', 'ban_ano', 'ban_titulo'),)


class Colaboradores(models.Model):
    co_id = models.AutoField(primary_key=True)
    co_pe_id1 = models.ForeignKey(
        'Pessoa', models.DO_NOTHING, db_column='co_pe_id1', related_name='da_pessoa')
    co_pe_id2 = models.ForeignKey(
        'Pessoa', models.DO_NOTHING, db_column='co_pe_id2', related_name='para_pessoa')


    class Meta:
        managed = False
        db_table = 'colaboradores'
        unique_together = (('co_pe_id1', 'co_pe_id2'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Orientacao(models.Model):
    ori_id = models.AutoField(primary_key=True)
    ori_pessoa = models.ForeignKey('Pessoa', models.DO_NOTHING)
    ori_aluno = models.TextField()
    ori_ano = models.IntegerField()
    ori_titulo = models.TextField()
    ori_pais = models.TextField(blank=True, null=True)
    ori_instituicao = models.TextField(blank=True, null=True)
    ori_agencia_fomento = models.TextField(blank=True, null=True)
    ori_tipo_orientacao = models.TextField(blank=True, null=True)
    ori_tipo_pesquisa = models.TextField(blank=True, null=True)
    ori_natureza = models.TextField(blank=True, null=True)
    ori_idioma = models.TextField(blank=True, null=True)
    ori_flag = models.TextField(blank=True, null=True)
    ori_doi = models.TextField(blank=True, null=True)
    ori_curso = models.TextField(blank=True, null=True)


    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in Orientacao.objects.all():
            title = tokenizer.tokenize(producao.ori_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywordsDep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects.filter(
            pe_departamento__contains=dep).values('pe_id_lattes')
        for producao in Orientacao.objects.filter(
            ori_pessoa__in=author_from_dep).distinct():
            title = tokenizer.tokenize(producao.ori_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    class Meta:
        managed = False
        db_table = 'orientacao'
        unique_together = (('ori_pessoa', 'ori_aluno', 'ori_titulo', 'ori_ano'),)


class Pessoa(models.Model):
    pe_id_lattes = models.BigIntegerField(primary_key=True)
    pe_nome_completo = models.TextField()
    pe_nome_citacao = models.TextField()
    pe_nacionalidade = models.TextField(blank=True, null=True)
    pe_contato = models.TextField(blank=True, null=True)
    pe_departamento = models.TextField()
    pe_cidade = models.TextField(blank=True, null=True)
    pe_estado = models.TextField(blank=True, null=True)
    pe_resumo = models.TextField(blank=True, null=True)
    
    producoes_bibliograficas = models.ManyToManyField(
        'ProducaoBibliografica', through='RelPessoaProdBib', related_name='producoes_bibliograficas')
    producoes_tecnicas = models.ManyToManyField(
        'ProducaoTecnica', through='RelPessoaProdTec', related_name='producoes_tecnica')
    producoes_artisticas = models.ManyToManyField(
        'ProducaoArtistica', through='RelPessoaProdArt', related_name='producoes_artisticas')
    aparicoes_bancas = models.ManyToManyField(
        'Bancas', through='RelPessoaBancas', related_name='aparicoes_bancas')
    participacoes = models.ManyToManyField(
        'Participacao', through='RelPessoaPart', related_name='participacoes')

    @staticmethod
    def keywords(id, limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        query_set = Pessoa.objects.filter(pe_id_lattes = id)
        if not query_set:
            return 'erro'
        for prod_art in query_set[0].producoes_artisticas.all():
            title = tokenizer.tokenize(prod_art.art_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        for prod_bib in query_set[0].producoes_bibliograficas.all():
            title = tokenizer.tokenize(prod_bib.bib_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        for prod_tec in query_set[0].producoes_tecnicas.all():
            title = tokenizer.tokenize(prod_tec.tec_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    class Meta:
        managed = False
        db_table = 'pessoa'


class PremiosTitulos(models.Model):
    pre_id = models.AutoField(primary_key=True)
    pre_pe = models.ForeignKey(Pessoa, models.DO_NOTHING)
    pre_nome = models.TextField()
    pre_ano = models.IntegerField()
    pre_entidade = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'premios_titulos'
        unique_together = (('pre_nome', 'pre_ano'),)


class ProducaoArtistica(models.Model):
    art_id = models.AutoField(primary_key=True)
    art_tipo = models.TextField()
    art_titulo = models.TextField()
    art_ano = models.IntegerField()
    art_autores = models.TextField(blank=True, null=True)
    art_natureza = models.TextField(blank=True, null=True)
    art_pais = models.TextField(blank=True, null=True)
    art_meio = models.TextField(blank=True, null=True)
    art_flag = models.TextField(blank=True, null=True)
    art_doi = models.TextField(blank=True, null=True)
    art_idioma = models.TextField(blank=True, null=True)
    art_premiacao = models.TextField(blank=True, null=True)
    art_exposicao = models.TextField(blank=True, null=True)
    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaProdArt', related_name='autores_artistica')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in ProducaoArtistica.objects.all():
            title = tokenizer.tokenize(producao.art_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywordsDep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects.filter(
            pe_departamento__contains=dep).values('pe_id_lattes')
        for producao in ProducaoArtistica.objects.filter(
            autores__in=author_from_dep).distinct():
            title = tokenizer.tokenize(producao.art_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    class Meta:
        managed = False
        db_table = 'producao_artistica'
        unique_together = (('art_tipo', 'art_titulo', 'art_ano'),)


class ProducaoBibliografica(models.Model):
    bib_id = models.AutoField(primary_key=True)
    bib_tipo = models.TextField()
    bib_titulo = models.TextField()
    bib_ano = models.IntegerField()
    bib_autores = models.TextField(blank=True, null=True)
    bib_publicacao = models.TextField(blank=True, null=True)
    bib_editora = models.TextField(blank=True, null=True)
    bib_edicao = models.TextField(blank=True, null=True)
    bib_volume = models.TextField(blank=True, null=True)
    bib_natureza = models.TextField(blank=True, null=True)
    bib_pais = models.TextField(blank=True, null=True)
    bib_idioma = models.TextField(blank=True, null=True)
    bib_meio = models.TextField(blank=True, null=True)
    bib_flag = models.TextField(blank=True, null=True)
    bib_doi = models.TextField(blank=True, null=True)
    bib_isbn = models.TextField(blank=True, null=True)
    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaProdBib', related_name='autores_bibliografica')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in ProducaoBibliografica.objects.all():
            title = tokenizer.tokenize(producao.bib_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywordsDep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects.filter(
            pe_departamento__contains=dep).values('pe_id_lattes')
        for producao in ProducaoBibliografica.objects.filter(
            autores__in=author_from_dep).distinct():
            title = tokenizer.tokenize(producao.bib_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    class Meta:
        managed = False
        db_table = 'producao_bibliografica'
        unique_together = (('bib_tipo', 'bib_titulo', 'bib_ano'),)


class ProducaoTecnica(models.Model):
    tec_id = models.AutoField(primary_key=True)
    tec_tipo = models.TextField()
    tec_titulo = models.TextField()
    tec_ano = models.IntegerField()
    tec_autores = models.TextField(blank=True, null=True)
    tec_natureza = models.TextField(blank=True, null=True)
    tec_pais = models.TextField(blank=True, null=True)
    tec_idioma = models.TextField(blank=True, null=True)
    tec_meio = models.TextField(blank=True, null=True)
    tec_flag = models.TextField(blank=True, null=True)
    tec_finalidade = models.TextField(blank=True, null=True)
    tec_pags = models.TextField(blank=True, null=True)
    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaProdTec', related_name='autores_tecnica')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in ProducaoTecnica.objects.all():
            title = tokenizer.tokenize(producao.tec_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywordsDep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects.filter(
            pe_departamento__contains=dep).values('pe_id_lattes')
        for producao in ProducaoTecnica.objects.filter(
            autores__in=author_from_dep).distinct():
            title = tokenizer.tokenize(producao.tec_titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in\
                stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    class Meta:
        managed = False
        db_table = 'producao_tecnica'
        unique_together = (('tec_tipo', 'tec_titulo', 'tec_ano'),)


class RelPessoaBancas(models.Model):
    pe_ban_id = models.AutoField(primary_key=True)
    pe_ban_pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING)
    pe_ban_bancas = models.ForeignKey(Bancas, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'rel_pessoa_bancas'
        unique_together = (('pe_ban_pessoa', 'pe_ban_bancas'),)

class RelPessoaProdArt(models.Model):
    pe_art_id = models.AutoField(primary_key=True)
    pe_art_pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING)
    pe_art_prod_art = models.ForeignKey(ProducaoArtistica, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'rel_pessoa_prod_art'
        unique_together = (('pe_art_pessoa', 'pe_art_prod_art'),)


class RelPessoaProdBib(models.Model):
    pe_bib_id = models.AutoField(primary_key=True)
    pe_bib_pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING)
    pe_bib_prod_bib = models.ForeignKey(ProducaoBibliografica, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'rel_pessoa_prod_bib'
        unique_together = (('pe_bib_pessoa', 'pe_bib_prod_bib'),)


class RelPessoaProdTec(models.Model):
    pe_tec_id = models.AutoField(primary_key=True)
    pe_tec_pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING)
    pe_tec_prod_tec = models.ForeignKey(ProducaoTecnica, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'rel_pessoa_prod_tec'
        unique_together = (('pe_tec_pessoa', 'pe_tec_prod_tec'),)
        
class Participacao(models.Model):
    par_id = models.AutoField(primary_key=True)
    par_nome_ev = models.TextField()
    par_tipo = models.TextField()
    par_ano = models.IntegerField()
    par_natureza = models.TextField(blank=True, null=True)
    par_pais = models.TextField(blank=True, null=True)
    par_idioma = models.TextField(blank=True, null=True)
    par_meio = models.TextField(blank=True, null=True)
    par_flag = models.TextField(blank=True, null=True)
    par_doi = models.TextField(blank=True, null=True)
    par_nome_inst = models.TextField(blank=True, null=True)
    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaPart', related_name='autores_participacao')

    class Meta:
        managed = False
        db_table = 'participacao'
        unique_together = (('par_tipo', 'par_nome_ev', 'par_ano'),)

class RelPessoaPart(models.Model):
    pe_par_id = models.AutoField(primary_key=True)
    pe_par_pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING)
    pe_par_participacao = models.ForeignKey(Participacao, models.DO_NOTHING)
    pe_par_titulo = models.TextField()

    class Meta:
        managed = False
        db_table = 'rel_pessoa_participacao'
        unique_together = (('pe_par_pessoa', 'pe_par_participacao'),)