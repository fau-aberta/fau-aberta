import os

from django.db import connection, migrations, models
import django.db.models.deletion


def load_data_from_sql(apps, schema_editor):
    file_path = os.path.join(os.path.dirname(
        __file__), 'DATABASE_DDL.sql')
    sql_statement = open(file_path).read()
    with connection.cursor() as c:
        c.execute(sql_statement)


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.RunPython(load_data_from_sql),
    ]
