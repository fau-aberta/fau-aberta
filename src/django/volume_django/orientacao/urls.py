from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /orientacao/
    url(r'^$', views.index, name='orientacao-index'),
    # ex: /orientacao/count
    url(r'^count$', views.count, name='orientacao-count'),
    # ex: /orientacao/keywords
    url(r'^keywords$', views.keywords, name='orientacao-keywords'),
    # ex: /orientacao/map
    url(r'^map$', views.map, name='orientacao-map'),
    # ex: /orientacao/tipos
    url(r'^tipos$', views.tipos, name='orientacao-tipos'),
    # ex: /orientacao/count_tipos
    url(r'^count_tipos$', views.count_tipos,
        name='orientacao-count_tipos'),
    # ex: /orientacao/rank
    url(r'^rank$', views.rank, name='orientacao-rank'),
]
