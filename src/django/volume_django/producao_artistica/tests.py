import json
from django.test import TestCase, Client
from django.urls import reverse

from lattes_data.models import ProducaoArtistica, RelPessoaProdArt


class ViewProducaoArtisticaTests(TestCase):
    """ Test module for producao_artistica views """
    # fixture file
    fixtures = [
                'util/fixtures/docentes.json', 
                'producao_artistica/fixtures/tests.json'
            ]

    # initialize the APIClient app
    client = Client()
        
    def test_index_list_format_returns_all_elements(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'format': 'list'})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        # returns 6 elements from 2017 -> 2019
        self.assertEqual(len(producao_list), 6)

    def test_index_default_format_with_valid_year_range_returns_all_years(self):
        # get API response
        response = self.client.get(
            reverse('producao_artistica-index'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)
        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        # returns 2 keys: 2018 and 2019
        self.assertEqual(len(producao_list), 2)

    def test_index_default_format_with_valid_year_range_returns_correct_element(self):
        # get API response
        response = self.client.get(
            reverse('producao_artistica-index'), {'ano_inicio': 1998, 'ano_fim': 1998})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(producao_list), 1)
    
    def test_index_default_format_for_specific_year_returns_all_elements(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(producao_list), 6)  

    def test_index_invalid_year_range_returns_404(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
        
    def test_index_invalid_year_returns_404(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_index_invalid_format_returns_404(self):
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'ano_fim': 'zebra'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_index_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()
        
        # get API response
        response = self.client.get(reverse('producao_artistica-index'), {'format': 'list'})
        json_response = json.loads(response.content)
        
        assert 'result' in json_response
        producao_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(producao_list), 0)
        
    def test_count_returns_correctly_for_each_department(self):
        response = self.client.get(reverse('producao_artistica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(prod_list[item]), 2)

    def test_count_returns_correctly_for_each_year(self):
        response = self.client.get(reverse('producao_artistica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        self.assertEqual(prod_list['AUH']['tipo1']['2011'], 1)
        self.assertEqual(prod_list['AUT']['tipo2']['2015'], 1)
        self.assertEqual(prod_list['AUH']['tipo1']['2015'], 1)

    def test_count_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 2017, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(prod_list[item]), 2)

    def test_count_returns_404_with_invalid_range(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_404_with_invalid_year(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_returns_default_format(self):
        response1 = self.client.get(reverse('producao_artistica-count'))
        json_response1 = json.loads(response1.content)
        response2 = self.client.get(
            reverse('producao_artistica-count'), {'format': 'default'})
        json_response2 = json.loads(response2.content)
        self.assertEqual(json_response1, json_response2)

    def test_count_returns_404_with_invalid_format(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 'zebra'})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        assert 'result' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_count_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-count'))
        json_response = json.loads(response.content)

        assert 'result' in json_response
        prod_list = json_response['result']
        assert 'code' in json_response

        self.assertEqual(json_response['code'], 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            years = prod_list[item]
            for value in years:
                self.assertEqual(years[value], 0)
                
    def test_keywords_with_default_limit(self):
        response = self.client.get(reverse('producao_artistica-keywords'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        
        assert 'result' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3},
            {"foo": 2},
            {"bar": 1}
        ]
        self.assertEqual(json_response['result'], expected_result)
        
    def test_keywords_with_specific_limit(self):
        response = self.client.get(reverse('producao_artistica-keywords'), {"limit": 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        
        assert 'result' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3}
        ]
        self.assertEqual(json_response['result'], expected_result)
    
    def test_keywords_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()
        
        response = self.client.get(reverse('producao_artistica-keywords'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)
        
        assert 'result' in json_response
        self.assertEqual(json_response['result'], [])
        
    def test_keywords_with_negative_limit_returns_404(self):
        response = self.client.get(reverse('producao_artistica-keywords'), { "limit": -1 })
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)
    
    def test_keywords_with_invalid_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_artistica-keywords'), {"limit": "foo"})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)
        
    def test_map_with_default_limit(self):
        response = self.client.get(reverse('producao_artistica-map'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {'Espanha': {'lat': '39.3262345', 'long': '-4.8380649', 'total': 3}},
            {'Brasil': {'lat': '-10.3333333', 'long': '-53.2', 'total': 3}}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_map_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": 2})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        expected_result = [
            {'Espanha': {'lat': '39.3262345', 'long': '-4.8380649', 'total': 3}},
            {'Brasil': {'lat': '-10.3333333', 'long': '-53.2', 'total': 3}}
        ]
        self.assertEqual(json_response['result'], expected_result)

    def test_map_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-map'))
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 200)

        assert 'result' in json_response
        self.assertEqual(json_response['result'], [])

    def test_map_with_negative_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": -1})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)

    def test_map_with_invalid_limit_returns_404(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": "foo"})
        json_response = json.loads(response.content)

        assert 'code' in json_response
        self.assertEqual(json_response['code'], 404)
