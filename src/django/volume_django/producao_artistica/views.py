from django.core.paginator import Paginator
from django.db.models import Count, Q
from django.http import JsonResponse
from util.format import make_chart_js
from lattes_data.models import Pessoa, ProducaoArtistica
import pickle
import datetime

now = datetime.datetime.now()
actual_year = str(now.year)

def error(msg):
    response_data = {
        'code': 404,
        'result': msg,
    }
    return JsonResponse(response_data)

def index(request):
    """Index for producao_artistica route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim', 'page'
    and 'page size' parameters.

    Returns:
    JsonResponse with status code and a list of artistic productions for each year.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    page = request.GET.get('page', '1')
    page_size = request.GET.get('page_size', '15')

    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    if not page.isdigit() or not page_size.isdigit():
        return error('Página ou tamanho de página inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    result = []
    query_set = ProducaoArtistica.objects.filter(
        art_ano__gte=int(ini_year), art_ano__lte=int(end_year)).order_by('-art_ano')
    production_page = Paginator(query_set, page_size)

    for production in production_page.page(page):
        authors = []
        for author in production.autores.all():
            authors.append({
                "nome": author.pe_nome_completo,
                "id_lattes": author.pe_id_lattes,
                "departamento": author.pe_departamento
            })
        result.append({
            "ano": production.art_ano,
            "titulo": production.art_titulo,
            "tipo": production.art_tipo,
            "autores": authors
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)


def count(request):
    """Count for producao_artistica route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamentos[]' and 'tipos[]' parameters.

    Returns:
    JsonResponse with status code and the count of artistic productions
    for each year for each departament.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['art_tipo']
                 for typ in ProducaoArtistica.objects.values('art_tipo').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit() or int(ini_year) < 1948:
        return error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {}
    for department in departments:
        result[department] = {}
        for type in types:
            result[department][type] = {}
            author_from_dep = Pessoa.objects.filter(
                pe_departamento__contains=department).values('pe_id_lattes')
            prods = ProducaoArtistica.objects.filter(
                art_tipo=type, autores__in=author_from_dep).distinct()

            for l in labels:
                count = prods.filter(art_ano=l).aggregate(Count('art_id'))
                result[department][type][l] = count['art_id__count']

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)


def keywords(request):
    """Keywords for producao_artistica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido.')

    if not limit > 0:
        return error('Limite inválido.')

    keywords = ProducaoArtistica.keywords(limit)
    result = []

    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})

    return JsonResponse({'code': 200, 'result': result})


def map(request):
    """Map for producao_artistica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries
    and the specific artistic productions counter.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return error('Limite inválido.')

    if not limit > 0:
        return error('Limite inválido.')

    countries = ProducaoArtistica.objects.all().values(
        'art_pais').annotate(total=Count('art_pais')).order_by('-total')[:limit]
    result = []

    with open('util/countries.pickle', 'rb') as file:
        country_dict = pickle.load(file)

    for country in countries:
        try:
            geolocation = country_dict[country['art_pais']]
            result.append({country['art_pais']: {'lat': geolocation['lat'],
                'long': geolocation['long'], 'total': country['total']}})
        except:
            pass

    return JsonResponse({'code': 200, 'result': result})

def tipos(request):
    """Tipos for producao_artistica route.

    Parameters: None

    Returns:
    JsonResponse with status code and a list of artistic production's types.
   """
    types = ProducaoArtistica.objects.all().values(
        'art_tipo').distinct().order_by('art_tipo')
    result = []
    accepted = ['Outra_Producao_Artistica_Cultural', 'Artes_Visuais', 'Musica']
    for t in types:
        if t['art_tipo'] in accepted:
            result.append(t['art_tipo'])
    return JsonResponse({'code': 200, 'result': result})

def count_tipos(request):
    """Count_tipos for producao_artistica route.

    Parameters: a GET request with 'tipos[]' and 'departamentos[]'.

    Returns:
    JsonResponse with status code and a list with the amount of each
    artistic production's type.
   """
    requested_types = request.GET.getlist('tipos[]')
    departments = request.GET.getlist('departamentos[]')

    query = ProducaoArtistica.objects.all().values(
        'art_tipo').distinct().order_by('art_tipo')
    valid_departments = ['AUH', 'AUT', 'AUP']

    valid_types = []
    for q in query:
        valid_types.append(q['art_tipo'])
    for t in requested_types:
        if t not in valid_types:
            return error('Algum tipo é inválido.')

    for dep in departments:
        if dep not in valid_departments:
            return error('Algum departamento inválido.')

    result = {}
    for dep in departments:
        values_list = []
        for t in requested_types:
            values = {}
            author_from_dep = Pessoa.objects.filter(
                        pe_departamento__contains=dep).values('pe_id_lattes')
            prods = ProducaoArtistica.objects.filter(
                        autores__in=author_from_dep).distinct().filter(
                        art_tipo=t).count()
            values[t] = prods
            values_list.append(values)
        result[dep] = values_list

    response_data = {
        "code": 200,
        "result": result,
    }
    return JsonResponse(response_data)

def rank(request):
    """Rank for producao_artistica route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'limit', 'tipos[]' and 'departamentos[]' parameters.

    Returns:
    JsonResponse with status code and a list of the <limit> teachers who created
    the most artistic productions and the respective number of creations.
   """

    try:
        limit = int(request.GET.get('limit', 10))
    except ValueError:
        return error('Limite inválido.')

    if not limit > 0:
        return error('Limite inválido.')

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    departments = request.GET.getlist('departamentos[]')
    types = request.GET.getlist('tipos[]')

    if not departments:
        departments = [dep['pe_departamento']
                       for dep in Pessoa.objects.values('pe_departamento').distinct()]
    if not types:
        types = [typ['art_tipo']
                 for typ in ProducaoArtistica.objects.values('art_tipo').distinct()]

    if not ini_year.isdigit() or not end_year.isdigit():
        return error('Ano inválido.')

    ini_year, end_year = int(ini_year), int(end_year)

    ranking = Pessoa.objects\
        .filter(Q(pe_departamento__in=departments) &
                    (Q(producoes_artisticas__art_tipo__in=types,
                       producoes_artisticas__art_ano__lte=int(end_year),
                       producoes_artisticas__art_ano__gte=int(ini_year)) |
                     Q(producoes_artisticas=None)))\
        .annotate(prod_count=Count('producoes_artisticas'))\
        .order_by('-prod_count')[:limit]

    result = []

    for value in ranking:
        result.append({
            "nome": value.pe_nome_completo,
            "numero_de_producoes": value.prod_count
        })

    response_data = {
        "code": 200,
        "result": result
    }

    return JsonResponse(response_data)
