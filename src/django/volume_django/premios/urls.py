from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /premios/
    url(r'^$', views.index, name='premios-index'),
    # ex: /premios/count
    url(r'^count/$', views.count, name='premios-count'),
    # ex: /premios/rank
    url(r'^rank/$', views.rank, name='premios-rank')
]
