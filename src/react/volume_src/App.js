import React from 'react'
import Home from './pages/Home'
import Header from './components/header'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Body } from './styles'
import Menu from './components/menu'
import Footer from './components/footer'
import ProdBibliografica from './pages/ProdBibliografica/ProdBibliografica'
import ProdBibliograficaAUH from './pages/ProdBibliografica/ProdBibliograficaAUH'
import ProdBibliograficaAUP from './pages/ProdBibliografica/ProdBibliograficaAUP'
import ProdBibliograficaAUT from './pages/ProdBibliografica/ProdBibliograficaAUT'
import ProdArtistica from './pages/ProdArtistica/ProdArtistica'
import ProdArtisticaAUH from './pages/ProdArtistica/ProdArtisticaAUH'
import ProdArtisticaAUP from './pages/ProdArtistica/ProdArtisticaAUP'
import ProdArtisticaAUT from './pages/ProdArtistica/ProdArtisticaAUT'
import ProdTecnica from './pages/ProdTecnica/ProdTecnica'
import ProdTecnicaAUH from './pages/ProdTecnica/ProdTecnicaAUH'
import ProdTecnicaAUP from './pages/ProdTecnica/ProdTecnicaAUP'
import ProdTecnicaAUT from './pages/ProdTecnica/ProdTecnicaAUT'
import Orientacoes from './pages/Orientacoes/Orientacoes'
import OrientacoesAUH from './pages/Orientacoes/OrientacoesAUH'
import OrientacoesAUP from './pages/Orientacoes/OrientacoesAUP'
import OrientacoesAUT from './pages/Orientacoes/OrientacoesAUT'
import Bancas from './pages/Bancas/Bancas'
import BancasAUH from './pages/Bancas/BancasAUH'
import BancasAUP from './pages/Bancas/BancasAUP'
import BancasAUT from './pages/Bancas/BancasAUT'
import Premios from './pages/Premios'
import About from './pages/About'
import ScrollToTop from './ScrollToTop'
import Docentes from './pages/Docentes'
import AUH from './pages/AUH'
import AUP from './pages/AUP'
import AUT from './pages/AUT'
import DocentesAUH from './pages/Docentes_AUH'
import DocentesAUP from './pages/Docentes_AUP'
import DocentesAUT from './pages/Docentes_AUT'
import ProfilePage from './pages/Perfil'
import GetProdB from './pages/ProducaoBibliografica'
import GetProdA from './pages/ProducaoArtistica'
import GetProdT from './pages/ProducaoTecnica'
import GetProdO from './pages/Orientacoes'
import GetProdBa from './pages/Bancas'
import GetProdP from './pages/PremiosETitulos'
import ProfileProdB from './pages/PerfilProdBiblio'
import ProfileProdA from './pages/PerfilProdArt'
import ProfileProdT from './pages/PerfilProdTec'
import ProfileOrientacoes from './pages/PerfilOrientacoes'
import ProfileBancas from './pages/PerfilBancas'
import ProfilePremios from './pages/PerfilPremios'

function App() {
    return (
        <Router onUpdate={() => console.log('oi')}>
            <Body>
                <React.Fragment>
                <Header />
                    <Menu />
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/pessoa/nomes' component={Docentes} />
                        <Route path='/AUH' component={AUH} />
                        <Route path='/pessoa/nome/auh' component={DocentesAUH} />
                        <Route path='/AUP' component={AUP} />
                        <Route path='/pessoa/nome/aup' component={DocentesAUP} />
                        <Route path='/AUT' component={AUT} />
                        <Route path='/pessoa/nome/aut' component={DocentesAUT} />
                        <Route path='/pessoa/perfil/' component={ProfilePage} />
                        <Route path='/producao-bibliografica' component={GetProdB} />
                        <Route path='/pessoa/ProduçãoBibliografica/' component={ProfileProdB} />
                        <Route path='/producao-bibliografica-auh' component={ProdBibliograficaAUH} />
                        <Route path='/producao-artistica' component={GetProdA} />
                        <Route path='/pessoa/ProduçãoArtística' component={ProfileProdA} />
                        <Route path='/producao-tecnica' component={GetProdT} />
                        <Route path='/pessoa/ProduçãoTécnica' component={ProfileProdT} />
                        <Route path='/bancas' component={GetProdBa} />
                        <Route path='/pessoa/Bancas' component={ProfileBancas} />
                        <Route path='/orientacoes' component={GetProdO} />
                        <Route path='/pessoa/Orientações' component={ProfileOrientacoes} />
                        <Route path='/premios' component={GetProdP} />
                        <Route path='/pessoa/PrêmioseTítulos' component={ProfilePremios} />
                    </Switch>
                </React.Fragment>
                <Footer />
            </Body>
            
        </Router>
    )
}

export default App