import React from 'react'
import { Pie } from 'react-chartjs-2'

function PieChart_no_legend({data = null}) {
    const options = {
        legend: { display: false },
        maintainAspectRatio: false,
        responsive: false,
        circumference: 0.5
    }

    return data && <Pie data={data} options={options} width={500} height={300} />
}

export default PieChart_no_legend