import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import styled from 'styled-components'
import LineChart from '../charts/line'
import Loading from './loading'
import { object } from 'prop-types'

function LineChartDocenteProdPremios({ dep, id, getDashboardCount, tipo, getCountDocente }) {
    const [lineData, setLineData] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        let data = {}
        getDashboardCount({ ano_inicio: 1978 , tipo: tipo, departamento: dep}).then(res => {
            data[dep] = res[dep]
        }).then(() => {
                    getCountDocente({ id: id }).then(resDoc => {
                        if(resDoc !== {}){
                            let aux = {}
                            let keys = Object.keys(resDoc)
                            keys.forEach(key => {
                                let doc_keys = Object.keys(resDoc[key])
                                doc_keys.forEach(year => {
                                    if(!aux[year]){
                                        aux[year] = resDoc[key][year]
                                    }
                                    else{
                                        aux[year] += resDoc[key][year] 
                                    }
                                })
                            }) 
                            data['DOCENTE'] = aux
                            setLineData(data)
                            setLoading(false)
                        }
                    })
            })
        }, [])

    return (
        <LineChartContainer>
            {loading ?
                <Loading />
                :
                <LineChart cachedData={lineData} changeLabels={'docente'} dep={dep} />
            }
        </LineChartContainer>
    )
}

const LineChartContainer = styled.div`
    padding: 20px;
    background-color: white;
`

export default LineChartDocenteProdPremios