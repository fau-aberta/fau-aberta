import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import MapChart from './world-map'
import Loading from './loading'

function MapDocente({ id, getMap, dep }) {
    const [map, setMap] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getMap({ id: id }).then(res => {
            setMap(res)
            setLoading(false)
        })
    }, [])

    return (   
        <div>
            {loading ?
            <Loading/>
            :
            <MapChart countries={map} dep={dep} />
        }
        </div>     
    )
}

export default MapDocente