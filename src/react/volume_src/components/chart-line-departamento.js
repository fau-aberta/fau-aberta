import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import styled from 'styled-components'
import LineChart from '../charts/line'
import Loading from './loading'

function LineChartDepartamento({ dep, getDashboardCount, getDepCount }) {
    const [lineData, setLineData] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        let data = {}
        getDashboardCount({ ano_inicio: 1978 }).then(res => {
            data['FAU'] = res['FAU']
        }).then(() => {
            getDepCount({ ano_inicio: 1978, departamento: dep }).then(res => {
                data[dep] = res[dep]
                setLineData(data)
                setLoading(false)
            })
        })
    }, [])

    return (
        <LineChartContainer>
            {loading ?
                <Loading />
                :
                <LineChart cachedData={lineData} changeLabels={'deps'} dep={dep} />
            }
        </LineChartContainer>
    )
}

const LineChartContainer = styled.div`
    padding: 20px;
    background-color: white;
`

export default LineChartDepartamento