import React, { useState } from 'react'
import styled from 'styled-components'
import Select from 'react-select'
import { dict } from '../dict'
import './css/dropdown.scss'

function Selector({ categories, setCategories, notSelected, setNotSelected }) {
    const [menuOpen, setMenuOpen] = useState(false)
    const options = notSelected.reduce((all, category) => {
        return [...all, { value: category, label: dict(category) }]
    }, [])

    const addCategory = ctg => {
        const id = notSelected.indexOf(ctg.value)
        setNotSelected([...notSelected.slice(0, id), ...notSelected.slice(id+1)])
        setCategories([...categories, ctg.value])
    }
    
    const removeCategory = ctg => {
        const id = categories.indexOf(ctg)
        setCategories([...categories.slice(0, id), ...categories.slice(id+1)])
        setNotSelected([...notSelected, ctg])
    }

    const DropdownIndicator = props => {
        return (
            <Add onClick={() => setMenuOpen(!menuOpen)}>+</Add>
        )
    }

    const Drop = ({ category }) => {
        return (
            <svg viewBox="0 0 20 12" width={20} height={9} onClick={() => removeCategory(category)}>
                <rect x={0} y={0} width={20} height={7} fill="black" />
            </svg>
        )
    }

    return (
        <div>
            <Container>
                {categories.map(category => {
                    return (
                        <Item key={category}>
                            <span>{dict(category)}</span>
                            <Drop category={category} />
                        </Item>
                    )
                })}
            </Container>
            <Button>
                <Select menuIsOpen={menuOpen} components={{ DropdownIndicator }} classNamePrefix 
                        value="foo" onChange={addCategory} options={options} />
            </Button>
        </div>
    )
}

export default Selector

const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    padding: 16px 8px 16px 8px;
    background-color: #E5E5E5;
    width: 220px;
    height: 206px;
    overflow-y: auto;
    margin: 32px 0px 0px 64px;
`
const Button = styled.div`
    margin-left: 64px;
    height: 64px;
    width: 236px;
    background-color: #E5E5E5;
    display: flex;
    justify-content: center;
    align-items: center;
`
const Item = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 8px;
    font-size: 18px;
    position: relative;
    > svg {
        cursor: pointer;
        position: absolute;
        right: 12px;
        margin-top: 2px;
    }
    > span {
        max-width: 150px;
        text-align: center;
    }
`

const Add = styled.span`
    font-size: 40px;
    font-weight: 900;
    background-color: #E5E5E5;
    cursor: pointer;
    user-select: none;
`