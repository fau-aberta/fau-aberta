import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import WordCloud from './word-cloud'
import Loading from './loading'

function KeywordsDocente({ id, getKeywords, dep }) {
    const [words, setWords] = useState([])
    const [loadingWords, setLoadingWords] = useState(true)

    useEffect(() => {
        getKeywords({ id: id, limit: 50 }).then(res => {
            setWords(res.map(obj => {
                return {
                    text: Object.keys(obj)[0],
                    value: Object.values(obj)[0]
                }
            }))
            setLoadingWords(false)
        })
    }, [])

    return (
        <div>
            {loadingWords ?
                <Loading  />
                :
                <WordCloud words={words} dep={dep} />
            }
        </div>
    )
}

export default KeywordsDocente