import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import styled from 'styled-components'
import LineChart from '../charts/line'
import Loading from './loading'

function LineChartProducaoFau({ getCount }) {
    const [lineData, setLineData] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        let data = {'FAU': {}}
        let aux = {}
        const departments = ['AUH', 'AUT', 'AUP']
        getCount({ ano_inicio: 1978 }).then(res => {
            departments.forEach(dep => {
                aux[dep] =  Object.values(res[dep]).reduce((prev, curr) => {
                    for (let key in curr) {
                        prev[key] = (prev[key] || 0) + curr[key];
                    }
                    return prev
                }, {});
            })
            departments.forEach(dep => {
                const keys = Object.keys(aux[dep]);
                keys.forEach(key => {
                    if (!data['FAU'][key]) {
                        data['FAU'][key] = aux[dep][key]
                    } else {
                        data['FAU'][key] += aux[dep][key]
                    }
                })
            })
            setLineData(data)
            setLoading(false)
        })
    }, [])

    return (
        <LineChartContainer>
            {loading ?
                <Loading />
                :
                <LineChart cachedData={lineData} changeLabels={'fau'} />                
            }
        </LineChartContainer>
    )
}

const LineChartContainer = styled.div`
    padding: 20px;
    background-color: white;
`

export default LineChartProducaoFau