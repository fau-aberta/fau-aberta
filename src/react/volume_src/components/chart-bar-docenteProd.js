import React, { useState, useEffect } from 'react'
import * as S from '../styles'
import styled from 'styled-components'
import StackedBar from '../charts/bar'
import 'rc-slider/assets/index.css'
import { dict } from '../dict'
import Loading from './loading'

function BarChartDocenteProducao({ dep, id, getCountByDep, getCountDocente, getTipos }) {
    const [allCategories, setAllCategories] = useState([])
    const [categories, setCategories] = useState([])
    const [barData, setBarData] = useState(null)
    const [allCount, setAllCount] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setCategories(allCategories)
    }, [allCategories])

    useEffect(() => {
        let data = {
            labels: ['DOCENTE'],
            datasets: categories.map(category => {
                return {
                    label: dict(category),
                    data: [],
                    value: category
                }
            })
        }
        
        data.datasets.forEach((obj, index) => {
            data.labels.forEach(label => {
                obj.backgroundColor = S.barColors[dep][index%6]
                obj.data.push(allCount[label][obj.value])
            })
        })
        setBarData(data)
    }, [categories])

    useEffect(() => {
        let count = {'DOCENTE': {}}
        getTipos({ id: id }).then(res => {
            getCountDocente({ id: id, 'tipos': res }).then(resDoc => {
                count['DOCENTE'] = resDoc
                setAllCount(count)
                setAllCategories(res)
                setLoading(false)
            })
        })
    }, [])

    return (
        <BarChartContainer>
            {loading ?
                <Loading />
                :
                <React.Fragment>
                    <StackedBar data={barData} type={'single'} />
                </React.Fragment>
            }
        </BarChartContainer>
    )
}

const BarChartContainer = styled.div`
    background-color: white;
    padding: 25px;
`

export default BarChartDocenteProducao
