import React from 'react'
import { useHistory  } from 'react-router-dom'
import {Container} from '../styles'
function Menu() {
let path = '/'
const history = useHistory();
const routeChange = () =>{ 
    history.push(path);
}
    function SetColor(e, event){setTimeout(()=>{
        let colorBtn = window.getComputedStyle(e).backgroundColor
        let colorText = window.getComputedStyle(e).color

        if(e.id === "FAU"){
            path = `/`
                routeChange()
            e.parentElement.querySelectorAll('a').forEach((a)=>{
            a.style.backgroundColor=""
            a.style.color="";
            a.style.border = "1px solid";
        })
        }

        if(e.id === "DOCENTES"){
            path = '/pessoa/nomes/'
            routeChange()
            const btnHome = document.querySelector('#FAU')
            btnHome.style.backgroundColor=""
            btnHome.style.color="";
            btnHome.style.border = "1px solid";
            document.querySelectorAll('a[id^=AU]').forEach((a)=>{
                    a.style.border = "2.5px solid"
                    a.style.backgroundColor=""
                    a.style.color="";
            document.querySelectorAll('#PUBLICACAO').forEach((a)=>{
                a.style.backgroundColor=""
                a.style.color="";
                a.style.border = "1px solid";
                })
                
    })
            
        }
        if(e.id === "AUH" || e.id === "AUP" || e.id === "AUT"){
            document.querySelectorAll('a[id^=AU]').forEach((a)=>{a.style.border = "1px solid"
            a.style.backgroundColor=""
            a.style.color= "#000"
        });
            document.querySelectorAll('#PUBLICACAO').forEach((a)=>{
                a.style.backgroundColor=""
                a.style.color="";
                a.style.border = "1px solid";
                })
        if(document.querySelector('#DOCENTES').style.backgroundColor == "rgb(0, 0, 0)"){
            document.querySelectorAll('a[id^=AU]').forEach((a)=>{a.style.border = "2.5px solid"
            a.style.backgroundColor=""
            a.style.color="#000"});
            
            path = `/pessoa/nome/${e.id}`
            routeChange() 
        }
        else{
            document.querySelectorAll('#PUBLICACAO').forEach((a)=>{
                a.style.backgroundColor=""
                a.style.color="";
                a.style.border = "1px solid";
                })
            path = `/${e.id}`
            routeChange() 
        }
        }

        if(e.id === "PUBLICACAO"){
           let filtroDep =  [...document.querySelectorAll('a[id^=AU]')].reduce((valid, a)=>{
                    if(a.style.border == "none"){
                        valid = true
                    }
                    else if (a.style.border == "2.5px solid"){
                        valid = true
                    }
                    
                    return valid
           }, false)  
           
           let filtroPub =  [...document.querySelectorAll('#PUBLICACAO')].reduce((valid, a)=>{
            if (a.style.border == "2.5px solid"){
                valid = true
            }
            
            return valid
   }, false)  
               
           if(filtroDep){
            e.parentElement.querySelectorAll('a').forEach((a)=>{
                a.style.backgroundColor=""
                a.style.color="";
                a.style.border = "1px solid";})
            }
            if(!filtroDep && filtroPub){
                let id = event.split('/')[5]
                path = `/pessoa/${e.innerText.replace(/\s+/g, "")}/${id}`
                routeChange() 
                
            }
            else{
                document.querySelectorAll('#PUBLICACAO').forEach((a)=>{
                    a.style.backgroundColor=""
                    a.style.color=""
                    a.style.border = "1px solid";
                    })
                path = `/${e.name}`
                routeChange()

            }

            
        }

        e.style.backgroundColor = colorBtn
        e.style.border = 'none'
        e.style.color = colorText
        
    })
    }


    return (
        <Container id = "links">
            <a to='/' id="FAU" onClick={event => SetColor(event.currentTarget)}>FAU Aberta</a>
            <a to='/pessoa/nomes' id="DOCENTES" onClick={event => SetColor(event.currentTarget)}>Docentes</a>
            <a to='#' id="AUH" onClick={event => SetColor(event.currentTarget)}>AUH</a>
            <a to='#' id="AUT" onClick={event => SetColor(event.currentTarget)}>AUT</a>
            <a to='#' id="AUP" onClick={event => SetColor(event.currentTarget)}>AUP</a>
            <a name='producao-bibliografica' id="PUBLICACAO" onClick={event => SetColor(event.currentTarget,window.location.href)}>Produção Bibliografica</a>
            <a name='producao-artistica' id="PUBLICACAO" onClick={event => SetColor(event.currentTarget, window.location.href)}>Produção Artística</a>
            <a name='producao-tecnica' id="PUBLICACAO" onClick={event => SetColor(event.currentTarget, window.location.href)}>Produção Técnica</a>
            <a name='orientacoes' id="PUBLICACAO" onClick={event => SetColor(event.currentTarget, window.location.href)}>Orientações</a>
            <a name='bancas' id="PUBLICACAO" onClick={event => SetColor(event.currentTarget, window.location.href)}>Bancas</a>
            <a name='premios' id="PUBLICACAO" onClick={event => SetColor(event.currentTarget, window.location.href)}>Prêmios e Títulos</a>
            

        </Container>
    )
}

export default Menu

const selected = {
    border: "1px solid",
    backgroundColor: "#fff",
    
}