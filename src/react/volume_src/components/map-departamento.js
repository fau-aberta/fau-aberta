import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import styled from 'styled-components'
import MapChart from './world-map'
import Loading from './loading'

function MapDepartamento({ dep, getMap }) {
    const [map, setMap] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getMap({ departamento: dep }).then(res => {
            setMap(res)
            setLoading(false)
        })
    }, [])

    return (   
        <MapContainer>
            {loading ?
                <Loading/>
                :
                <MapChart countries={map} dep={dep} />
        }
        </MapContainer>     
    )
}

const MapContainer = styled.div`
    background-color: white;
`

export default MapDepartamento