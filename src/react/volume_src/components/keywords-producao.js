import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import styled from 'styled-components'
import WordCloud from './word-cloud'
import Loading from './loading'

function KeywordsProducao({ getKeywords }) {
    const [words, setWords] = useState([])
    const [loadingWords, setLoadingWords] = useState(true)

    useEffect(() => {
        getKeywords({ limit: 50 }).then(res => {
            setWords(res.map(obj => {
                return {
                    text: Object.keys(obj)[0],
                    value: Object.values(obj)[0]
                }
            }))
            setLoadingWords(false)
        })
    }, [])

    return (
        <KeywordsContainer>
            {loadingWords ?
                <Loading />
                :
                <WordCloud words={words} />
            }
        </KeywordsContainer>
    )
}

const KeywordsContainer = styled.div`
    background-color: white;
    padding-bottom: 10px;
    height: 100%;
`

export default KeywordsProducao