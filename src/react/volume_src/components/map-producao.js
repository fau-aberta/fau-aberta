import React, { useState, useEffect } from 'react'
import 'rc-slider/assets/index.css'
import styled from 'styled-components'
import MapChart from './world-map'
import Loading from './loading'

function MapProducao({ getMap }) {
    const [map, setMap] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getMap().then(res => {
            setMap(res)
            setLoading(false)
        })
    }, [])

    return (   
        <MapContainer>
            {loading ?
                <Loading/>
                :
                <MapChart countries={map}/>
        }
        </MapContainer>     
    )
}

const MapContainer = styled.div`
    background-color: white;
`

export default MapProducao