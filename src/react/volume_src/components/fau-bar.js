import React from 'react'
import styled from 'styled-components'

function FauBar() {
    return (
        <Bar>
            <a href="http://www.fau.usp.br/" target="_blank" rel="noopener noreferrer">Site da FAU-USP</a>
        </Bar>
    )
}

export default FauBar

const Bar = styled.div`
    width: 100%;
    height: 32px;
    background-color: black;
    position: fixed;
    top: 0px;
    > a {
        color: white;
        margin-right: 32px;
        text-decoration: none;
        font-size: 18px;
        :hover {
            text-decoration: underline;
        }
    }
    display: flex;
    justify-content: flex-end;
    align-items: center;
    font-family: sans-serif;
`