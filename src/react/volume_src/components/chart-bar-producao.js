import React, { useState, useEffect } from 'react'
import * as S from '../styles'
import styled from 'styled-components'
import StackedBar from '../charts/bar'
import 'rc-slider/assets/index.css'
import { dict } from '../dict'
import Loading from './loading'

function BarChartProducao({ getCount, getTipos}) {
    const [allCategories, setAllCategories] = useState([])
    const [categories, setCategories] = useState([])
    const [barData, setBarData] = useState(null)
    const [allCount, setAllCount] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setCategories(allCategories)
    }, [allCategories])

    useEffect(() => {
        let data = {
            labels: ['AUH', 'AUT', 'AUP'],
            datasets: categories.map(category => {
                return {
                    label: dict(category),
                    data: [],
                    value: category
                }
            })
        }
        
        data.datasets.forEach((obj, index) => {
            data.labels.forEach(dep => {
                obj.backgroundColor = S.barColors['FAU'][index%6]
                obj.data.push(Object.values(allCount[dep][obj.value]).reduce((a, b) => a + b))
            })
        })
        setBarData(data)
    }, [categories])

    useEffect(() => {
        getTipos().then(res => {
            getCount({ tipos: res }).then(count => {
                setAllCount(count)
                setAllCategories(res)
                setLoading(false)
            })
        })
    }, [])

    return (
        <BarChartContainer>
            {loading ?
                <Loading />
                :
                <React.Fragment>
                    <StackedBar data={barData} />
                </React.Fragment>
            }
        </BarChartContainer>
    )
}

const BarChartContainer = styled.div`
    background-color: white;
    padding: 25px;
`

export default BarChartProducao