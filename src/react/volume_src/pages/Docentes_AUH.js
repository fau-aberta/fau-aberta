import React, {useEffect,useState} from 'react'
import * as S from '../styles'
import {DivLinkDocentes} from '../styles'
import { getNomesPessoa} from '../api'
import { useHistory } from 'react-router-dom'
import { Link  } from 'react-router-dom'
import {setColorDocentesAUH} from '../components/btnFunctions'
import Loading from '../components/loading'


let docentes = []

const pessoaAUH = (user) => user.forEach(({nome,departamento,id})=>{
    if(id.length <= 15){
        id = `0${id}`
    }
    docentes.push([nome,departamento,id])
})


function DocentesAUH(){
    const history = useHistory()
    const handleButtonClick = (event) => {
    
        history.push(event.currentTarget.id)
      }
      let [user, setUser] = useState(null);
      useEffect(() => {
        setColorDocentesAUH()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getNomesPessoa().then(u => setUser(u))
        })();
    }, []);

    if ( user == null) {
        return <S.CenteredColumn>
              {Loading()}
        </S.CenteredColumn>;
      }
      
    docentes = []
    pessoaAUH(user) 
    return( 

        <S.CenteredColumn style={{justifyContent: 'start'}}>
            <DivLinkDocentes>{docentes.map((people) => {
            let [p, dep,id] = people;
            let redirect = `/pessoa/perfil/`
            if(dep === 'AUH'){
            return < span id={id} key={p} onClick={handleButtonClick}><Link to = {redirect} >{p}</Link></span>
            }
            else{
                return < span id={id} key={p} ><a style={{color: '#bbb'}} >{p}</a></span>
            }
            })}
          </DivLinkDocentes>
        </S.CenteredColumn>
        
    )
}

export default DocentesAUH

