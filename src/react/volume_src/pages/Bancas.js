import React, {
    useState,
    useEffect
  } from "react";
import { getDashboard, getBancasKeywords, getBancasMap, getBancasCount, getBancasTipos } from '../api'
import {Conteudo,DivCard,DivGraph,DivInfoText,DivInfoProducao,DivTotal,DivTitle} from '../styles'
import {setColorBanca} from '../components/btnFunctions' 
import Loading from '../components/loading'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import KeywordsProducao from '../components/keywords-producao'
import MapProducao from '../components/map-producao'
import BarChartProducao from '../components/chart-bar-producao'
import LineChartProducao from '../components/chart-line-producao'
import LineChartProducaoFau from '../components/chart-line-producao-fau'
import {Bancas} from '../static_texts'

function GetProdBa() {
    const [dados, setDados] = useState(null);

    useEffect(() => {
        setColorBanca()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getDashboard().then(u => setDados(u))
        })();
    }, []);


  
    if (dados == null) {
        return <Conteudo style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>

        <> {Loading()}</>

      </Conteudo>;
    }
    return (
        
      <>
        <ProdBa Total = {dados}/>
      </>
    );
  }


function ProdBa(props){
    let [totalizacao] = Object.values(props)
    let {['Bancas']: bancas } = totalizacao
    return(
        
        <Conteudo style={{justifyContent: 'start'}}>
            <DivCard style={{border: 'none'}}>
                <DivInfoProducao>
                    <DivTitle>
                        <span >Bancas</span>
                    </DivTitle>
                    <DivTotal>
                        <span style={{fontSize: 100}}>{Intl.NumberFormat().format(bancas)}</span>
                        <span>resultados</span>
                    </DivTotal>
                </DivInfoProducao>
                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false}showIndicators={false} showStatus={false} autoPlay={true} infiniteLoop={true} interval={5000} >
                        <div>
                            <KeywordsProducao
                                getKeywords={getBancasKeywords}
                            />
                        </div>
                        <div>
                            <MapProducao
                                getMap={getBancasMap}
                            />
                        </div>
                        <div >
                            <BarChartProducao
                                getCount={getBancasCount}
                                getTipos={getBancasTipos}
                            />
                        </div>
                        <div>
                            <LineChartProducaoFau
                                getCount={getBancasCount}
                            />
                        </div>
                        <div>
                            <LineChartProducao
                                getCount={getBancasCount}
                            />
                        </div>
                    </Carousel>
                </DivGraph>  
                <DivInfoText>
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Apresentação</span>
                    <span>{Bancas}</span>
                </DivInfoText>  
          </DivCard>
        </Conteudo>
        
    )
}

export default GetProdBa

