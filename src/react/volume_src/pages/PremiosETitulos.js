import React, {
    useState,
    useEffect
  } from "react";
import { getDashboard, getPremiosCount } from '../api'
import {Conteudo,DivCard,DivGraph,DivInfoText,DivInfoProducao,DivTotal,DivTitle} from '../styles'
import {setColorPremio} from '../components/btnFunctions' 
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Loading from '../components/loading'
import LineChartProducao from '../components/chart-line-producao'
import LineChartProducaoFau from '../components/chart-line-producao-fau'
import {Premios} from '../static_texts'

function GetProdP() {
    const [dados, setDados] = useState(null);

    useEffect(() => {
        setColorPremio()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getDashboard().then(u => setDados(u))
        })();
    }, []);


  
    if (dados == null) {
        return <Conteudo style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>

        <> {Loading()}</>

      </Conteudo>;
    }
    return (
        
      <>
        <ProdP Total = {dados}/>
      </>
    );
  }


function ProdP(props){
    let [totalizacao] = Object.values(props)
    let {['Prêmios e Títulos']: premios } = totalizacao
    return(
        
        <Conteudo style={{justifyContent: 'start'}}>
            <DivCard style={{border: 'none'}}>
                <DivInfoProducao>
                    <DivTitle>
                        <span >Prêmios e Títulos</span>
                    </DivTitle>
                    <DivTotal>
                        <span style={{fontSize: 100}}>{Intl.NumberFormat().format(premios)}</span>
                        <span>resultados</span>
                    </DivTotal>
                </DivInfoProducao>
                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false} showIndicators={false} showStatus={false} autoPlay={true} infiniteLoop={true} interval={5000} >
                        <div>
                            <LineChartProducaoFau
                                getCount={getPremiosCount}
                            />
                        </div>
                        <div>
                            <LineChartProducao
                                getCount={getPremiosCount}
                            />
                        </div>
                    </Carousel>
                </DivGraph>  
                <DivInfoText>
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Apresentação</span>
                    <span>{Premios}</span>
                </DivInfoText>  
          </DivCard>
        </Conteudo>
        
    )
}

export default GetProdP
