import React, {
    useState,
    useEffect
  } from "react";
import { getDashboard, getProdBibKeywords, getProdBibMap, getProdBibCount, getProdBibTipos } from '../api'
import {Conteudo,DivCard,DivGraph,DivInfoText,DivInfoProducao,DivTotal,DivTitle} from '../styles'
import {ProdBib} from '../static_texts'
import {setColorProducaoB} from '../components/btnFunctions' 
import Loading from '../components/loading'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import KeywordsProducao from '../components/keywords-producao'
import MapProducao from '../components/map-producao'
import BarChartProducao from '../components/chart-bar-producao'
import LineChartProducao from '../components/chart-line-producao'
import LineChartProducaoFau from '../components/chart-line-producao-fau'

function GetProdB() {
    const [dados, setDados] = useState(null);

    useEffect(() => {
        setColorProducaoB()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getDashboard().then(u => setDados(u))
        })();
    }, []);


    if (dados == null) {
      return <Conteudo style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <> {Loading()}</>
      </Conteudo>;
    }
    return (
        
      <>
        <ProdB Total = {dados}/>
      </>
    );
  }


function ProdB(props){
    let [totalizacao] = Object.values(props)
    let {['Produção Bibliográfica']: prodB } = totalizacao
    return(
        
        <Conteudo style={{justifyContent: 'start'}}>
            <DivCard style={{border: 'none'}}>
                <DivInfoProducao>
                    <DivTitle>
                        <span >Produção Bibliográfica</span>
                    </DivTitle>
                    <DivTotal>
                        <span style={{fontSize: 100}}>{Intl.NumberFormat().format(prodB)}</span>
                        <span>resultados</span>
                    </DivTotal>
                </DivInfoProducao>
                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false}showIndicators={false} autoPlay={true} showStatus={false}  infiniteLoop={true} interval={5000} >
                        <div>
                            <KeywordsProducao
                                getKeywords={getProdBibKeywords}
                            />
                        </div>
                        <div>
                            <MapProducao
                                getMap={getProdBibMap}
                            />
                        </div>
                        <div>
                            <BarChartProducao
                                getCount={getProdBibCount}
                                getTipos={getProdBibTipos}
                            />
                        </div>
                        <div>
                            <LineChartProducaoFau
                                getCount={getProdBibCount}
                            />
                        </div>
                        <div>
                            <LineChartProducao
                                getCount={getProdBibCount}
                            />
                        </div>
                    </Carousel>
                </DivGraph>  
                <DivInfoText>
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Apresentação</span>
                    <span>{ProdBib}</span>
                </DivInfoText>  
          </DivCard>
        </Conteudo>
        
    )
}

export default GetProdB
