import React, { useState, useEffect } from 'react'
import Table from '../components/table'
import * as S from '../styles'
import { getPremiosRank } from '../api'
import Loading from '../components/loading'
import styled from 'styled-components'

function Premios() {
    const [ranking, setRanking] = useState(null)
    const [limit, setLimit] = useState(10)
    const [loading, setLoading] = useState(true)

    useEffect(() => getRank(), [])

    let col1, col2

    if (ranking) {
        col1 = ranking.reduce((prev, cur) => {
            return [...prev, `${cur[0]} (${cur[1]})`]
        }, [])
        col2 = ranking.reduce((prev, cur) => {
            return [...prev, cur[2]]
        }, [])
    }

    const getRank = () => {
        setLoading(true)
        getPremiosRank({ limit }).then(res => setRanking(res))
        setLoading(false)
    }

    return (
        <S.CenteredColumn>
            <S.Title style={{marginTop: 48}}>PRÊMIOS</S.Title>

            <Row>
                <span>Digite o número de docentes: </span>
                <Input value={limit} onChange={e => setLimit(Number(e.target.value))}
                        onKeyPress={e => e.key === 'Enter' && getRank()} />
                <span onClick={getRank}>Consultar</span>
            </Row>
    
            {!loading && col1 && col2 ?
                <Table title1="Nome Docente" title2="Qtd. de Prêmios" col1={col1} col2={col2} />
                :
                <Center><Loading /></Center>
            }
        </S.CenteredColumn>
    )
}

export default Premios

const Input = styled.input`
    height: 24px;
    font-size: 20px;
    text-align: center;
    width: 80px;
    margin: 0px 16px 16px 8px;
`
const Row = styled.div`
    display: flex;
    align-items: center;
    > span {
        margin-top: -12px;
        font-size: 18px;
        height: 30px;
        :nth-child(3) {
            text-decoration: underline;
            color: #383838;
            :hover {
                cursor: pointer;
            }
        }
    }
`
const Center = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 400px;
`