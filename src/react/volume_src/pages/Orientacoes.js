import React, {
    useState,
    useEffect
  } from "react";
import { getDashboard, getOrientacaoKeywords, getOrientacaoMap, getOrientacaoCount, getOrientacaoTipos } from '../api'
import {setColorOrientacao} from '../components/btnFunctions' 
import {Conteudo,DivCard,DivGraph,DivInfoText,DivInfoProducao,DivTotal,DivTitle} from '../styles'
import Loading from '../components/loading'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import KeywordsProducao from '../components/keywords-producao'
import MapProducao from '../components/map-producao'
import BarChartProducao from '../components/chart-bar-producao'
import LineChartProducao from '../components/chart-line-producao'
import LineChartProducaoFau from '../components/chart-line-producao-fau'
import {Orientacao} from '../static_texts'

function GetProdO() {
    const [dados, setDados] = useState(null);

    useEffect(() => {
        setColorOrientacao()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getDashboard().then(u => setDados(u))
        })();
    }, []);


  
    if (dados == null) {
        return <Conteudo style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>

        <> {Loading()}</>
      </Conteudo>;
    }
    return (
        
      <>
        <ProdO Total = {dados}/>
      </>
    );
  }


function ProdO(props){
    let [totalizacao] = Object.values(props)
    let {['Orientação']: orientacao } = totalizacao
    return(
        
        <Conteudo style={{justifyContent: 'start'}}>
            <DivCard style={{border: 'none'}}>
                <DivInfoProducao>
                    <DivTitle>
                        <span>Orientações</span>
                    </DivTitle>
                    <DivTotal>
                        <span style={{fontSize: 100}}>{Intl.NumberFormat().format(orientacao)}</span>
                        <span>resultados</span>
                    </DivTotal>
                </DivInfoProducao>
                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false}showIndicators={false} showStatus={false} autoPlay={true} infiniteLoop={true} interval={5000} >
                        <div>
                            <KeywordsProducao
                                getKeywords={getOrientacaoKeywords}
                            />
                        </div>
                        <div>
                            <MapProducao
                                getMap={getOrientacaoMap}
                            />
                        </div>   
                        <div>
                            <BarChartProducao
                                getCount={getOrientacaoCount}
                                getTipos={getOrientacaoTipos}
                            />
                        </div>
                        <div>
                            <LineChartProducaoFau
                                getCount={getOrientacaoCount}
                            />
                        </div>
                        <div>
                            <LineChartProducao
                                getCount={getOrientacaoCount}
                            />
                        </div>
                    </Carousel>
                </DivGraph>  
                <DivInfoText>
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Apresentação</span>
                    <span>{Orientacao}</span>
                </DivInfoText>  
          </DivCard>
        </Conteudo>
        
    )
}

export default GetProdO
