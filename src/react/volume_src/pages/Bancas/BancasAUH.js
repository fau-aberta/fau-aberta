import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getBancasCount, getBancasTipos, getBancasRank } from '../../api'

function BancasAUH() {
    return (
        <ChartsDepartment 
            title="Bancas" 
            department="AUH"
            getCount={getBancasCount}
            getTipos={getBancasTipos}
            getRank={getBancasRank}
        />
    )
}

export default BancasAUH