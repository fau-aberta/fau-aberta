import React, {
    useState,
    useEffect
  } from "react";
import {Close,Conteudo,DivCard,DivInfo,DivGraph,DivInfoText,DivDep,DivInfoProf,DivLink} from '../styles'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { getImg, getPessoa, getProdArtCountTipos, getCountProdArtPessoa, getTiposProdArtPessoa, getDashboardCount, getDashboardCountByDepTipo, getCountProdArtPessoaPorAno } from '../api'
import {Link, useHistory } from 'react-router-dom'
import Loading from '../components/loading'
import {setFilterPerfilPA} from '../components/btnFunctions'
import imageAvatar from '../assets/lattes_avatar_preto.png'
import BarChartDocenteProducao from '../components/chart-bar-docenteProd'
import LineChartDocenteProd from '../components/chart-line-docenteProd'

function ProfileProdA() {

    function getMeta(url){   
        
        var img = new Image();
        img.addEventListener("load", function(){
            try{
            let img = document.querySelector('#image');
            img.style.backgroundAttachment = '';
            img.innerText = '';
            let color = [...document.querySelectorAll('a[id^=AU]')].filter(x=>x.style.backgroundColor)[0].style.backgroundColor;
            img.style.border = `8px solid ${color}`

            if((this.naturalWidth === this.naturalHeight) || ((this.naturalWidth < this.naturalHeight+10) && this.naturalWidth > this.naturalHeight-10)){
                img.style.backgroundPosition = 'center 0'
            }
            else if(this.naturalWidth > this.naturalHeight){
                img.style.backgroundSize = 'auto 145px '
                img.style.backgroundPosition = 'center 0'
            }
            if(this.naturalWidth < 200){
                img.style.backgroundPosition = 'center 0'
            }
            if(this.naturalWidth == 95 && this.naturalHeight == 99){
                        img.style.backgroundImage = `url(${imageAvatar})`
            }
            if(this.naturalWidth >750){
                img.style.backgroundSize = '140px auto'
    }
        } catch(e){
            return imageAvatar
        }
        });
        img.src = url;
        
        return img.src

    }

    let [user, setUser] = useState(null);
    const [dataUser, setDataUser] = useState(null);
    const history = useHistory()
    let {pathname} = history.location
    let idLattes = pathname.split('/')[3]
    useEffect( () => {
        (async function getData(){
        return await getImg(idLattes).then(u=> getMeta(u) ).then(u => setUser(u))
        })();
    }, []);
    useEffect( () => {
        (async function getDataUser(){
        return await getPessoa({id:idLattes}).then(u => setDataUser(u))
        })();
    }, []);
    if ( user == null || dataUser == null) {
      return <Conteudo style={{alignSelf: 'center', textAlign: 'center'}}>
            {Loading()}
      </Conteudo>;
    }

    return (
        
      <>
        <Perfil foto = {user} dados = {dataUser} id={idLattes}/>
      </>
    );
  }


function Perfil(props){
   let prod = []
   let sorted =  props.dados.producoes_artisticas.sort((a, b) => Number(b.ano) - Number(a.ano));
   if(sorted.length === 0){
    prod.push(<div style={{ margin: "20px 0px", display: 'flex', flexDirection: 'column'}}><span style={{marginTop: -20}}><b>0 resultados</b></span></div>)
   } 
   let groupBy = (array, key) => {
    return array.reduce((result, obj) => {
       (result[obj[key]] = result[obj[key]] || []).push(obj);
       return result;
    }, {});
 };
     let g = groupBy(sorted, "ano") 
     
     
    for(let i in g || []){
        for(let h of g[i]){  
            
            prod.push(<div style={{ margin: "20px 0px", display: 'flex', flexDirection: 'column', fontSize:15}}>
            <span><b>Título:</b> {h.titulo || "Não informado"}</span>
            <span><b>DOI:</b> {h.doi || "Não informado"}</span>
            <span><b>Autores:</b> {h.autores.join(", ") || "Não informado"}</span>
            <span><b>País:</b> {h.pais || "Não informado"}</span>
            </div>)
        }
        prod.push(<span id="ano">{i}</span>)
        

    }
    
    setFilterPerfilPA(props.dados.departamento)
    return(
        
        <Conteudo style={{justifyContent: 'start'}}>
            <DivCard>
                <DivInfo>
                <DivDep id="image" style={{fontSize: 12, backgroundImage: `url(${props.foto})`, backgroundAttachment: 'fixed'}}>
                        {Loading()}
                    </DivDep>
                    <DivInfoProf>
                        <span style={{fontSize: 25}}>{props.dados.nome_completo}</span>
                        <span>Telefone</span>
                        {props.dados.contato ? 
                        <span>+55 (11) {props.dados.contato}</span> :
                        <span>-</span>
                        }
                        <span>Currículo Lattes</span>
                        <a href={`http://lattes.cnpq.br/${props.dados.id_lattes}`} target="_blank">http://lattes.cnpq.br/{props.dados.id_lattes}</a>
                    </DivInfoProf>
                </DivInfo>
                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false}showIndicators={false} showStatus={false} autoPlay={true} infiniteLoop={true} interval={5000} >
                        <div>
                            <BarChartDocenteProducao
                                id={props.dados.id_lattes}
                                dep={props.dados.departamento}
                                getCountByDep={getProdArtCountTipos}
                                getCountDocente={getCountProdArtPessoa}
                                getTipos={getTiposProdArtPessoa}
                            />
                        </div>
                        <div>
                            <LineChartDocenteProd
                                dep={props.dados.departamento}
                                id={props.dados.id_lattes}
                                getDashboardCount={getDashboardCountByDepTipo}
                                tipo={'Produção Artística'}
                                getCountDocente={getCountProdArtPessoaPorAno}
                                getTipos={getTiposProdArtPessoa}
                            />
                        </div>
                    </Carousel>
                </DivGraph>
                <DivInfoText id = "scroll">
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Produção Artística
                    <Link to={`/pessoa/perfil/${props.id}`} style={Close}>X</Link>
                    </span>
                    <DivLink>{prod.reverse().map((people,index) => {
                    return <div key={index}>
                        {people}
                        </div>
                    })} 
                </DivLink>


                </DivInfoText>  

          </DivCard>
        </Conteudo>
        
    )
}

export default ProfileProdA


