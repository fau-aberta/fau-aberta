import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getOrientacaoCount, getOrientacaoTipos, getOrientacaoRank } from '../../api'

function OrientacoesAUH() {
    return (
        <ChartsDepartment 
            title="Orientações" 
            department="AUH"
            getCount={getOrientacaoCount}
            getTipos={getOrientacaoTipos}
            getRank={getOrientacaoRank}
        />
    )
}

export default OrientacoesAUH