import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdTecCount, getProdTecTipos, getProdTecRank } from '../../api'

function ProdTecnicaAUP() {
    return (
        <ChartsDepartment
            title="Produção Técnica"
            department="AUP"
            getCount={getProdTecCount}
            getTipos={getProdTecTipos}
            getRank={getProdTecRank}
        />
    )
}

export default ProdTecnicaAUP