import React, {useEffect,    useState} from 'react'
import * as S from '../styles'
import {DivLinkDocentes} from '../styles'
import { Link  } from 'react-router-dom'
import { getNomesPessoa} from '../api'
import { useHistory } from 'react-router-dom'
import {setColorDocentes} from '../components/btnFunctions'
import Loading from '../components/loading'

let docentes = []
const listaDocentes = (user) => user.forEach(({id,nome})=>{
    if(id.length <= 15){
        id = `0${id}`
    }
    docentes.push([id,nome])
})


 
function Docentes(){
    const history = useHistory()
    const handleButtonClick = (event) => {
    
        history.push(event.currentTarget.id)
      }
    let [user, setUser] = useState(null);

    useEffect(() => {
        setColorDocentes()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getNomesPessoa().then(u => setUser(u))
        })();
    }, []);

    if ( user == null) {
        return <S.CenteredColumn>
              {Loading()}
        </S.CenteredColumn>;
      }
    docentes = []
    listaDocentes(user)
    return(

        < S.CenteredColumn style={{justifyContent: 'start'}}>
                <DivLinkDocentes>{docentes.map((people) => {
                    
                    let [id, nome] = people;
                    let redirect = `/pessoa/perfil/`
                    return <span id = {id} key={nome} onClick={handleButtonClick} ><Link to = {redirect} >{nome}</Link></span>
                    })}
                </DivLinkDocentes>
        </S.CenteredColumn>
        
    )
}


export default Docentes