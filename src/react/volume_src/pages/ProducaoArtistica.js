import React, {
    useState,
    useEffect
  } from "react";
import { getDashboard, getProdArtKeywords, getProdArtMap, getProdArtCount, getProdArtTipos } from '../api'
import {Conteudo,DivCard,DivGraph,DivInfoText,DivInfoProducao,DivTotal,DivTitle} from '../styles'
import {ProdArt} from '../static_texts'
import {setColorProducaoA} from '../components/btnFunctions' 
import Loading from '../components/loading'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import KeywordsProducao from '../components/keywords-producao'
import MapProducao from '../components/map-producao'
import BarChartProducao from '../components/chart-bar-producao'
import LineChartProducao from '../components/chart-line-producao'
import LineChartProducaoFau from '../components/chart-line-producao-fau'


function GetProdA() {
    const [dados, setDados] = useState(null);
    
    useEffect(() => {
        setColorProducaoA()
    }, [])

    useEffect( () => {
        (async function getData(){
        return await getDashboard().then(u => setDados(u))
        })();
    }, []);


  
    if (dados == null) {
        return <Conteudo style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>

        <> {Loading()}</>

      </Conteudo>;
    }
    return (
        
      <>
    
        <ProdA  p = {dados}/>
      </>
    );
  }


function ProdA(props){
    let [totalizacao] = Object.values(props)
    let {['Produção Artística']: prodA } = totalizacao
    return(
        
        <Conteudo style={{justifyContent: 'start'}}>
            <DivCard style={{border: 'none'}}>
                <DivInfoProducao>
                    <DivTitle>
                        <span>Produção Artística</span>
                    </DivTitle>
                    <DivTotal>
                        <span style={{fontSize: 100}}>{Intl.NumberFormat().format(prodA)}</span>
                        <span>resultados</span>
                    </DivTotal>
                </DivInfoProducao>
                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false}showIndicators={false} showStatus={false} autoPlay={true} infiniteLoop={true} interval={5000} >
                        <div>
                            <KeywordsProducao
                                getKeywords={getProdArtKeywords}
                            />
                        </div>
                        <div>
                            <MapProducao
                                getMap={getProdArtMap}
                            />
                        </div>
                        <div>
                            <BarChartProducao
                                getCount={getProdArtCount}
                                getTipos={getProdArtTipos}
                            />
                        </div>
                        <div>
                            <LineChartProducaoFau
                                getCount={getProdArtCount}
                            />
                        </div>
                        <div>
                            <LineChartProducao
                                getCount={getProdArtCount}
                            />
                        </div>                        
                    </Carousel>
                </DivGraph>  
                <DivInfoText>
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Apresentação</span>
                    <span>{ProdArt}</span>
                </DivInfoText>  
          </DivCard>
        </Conteudo>
        
    )
}

export default GetProdA
