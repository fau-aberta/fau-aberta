import React, {
    useState,
    useEffect
  } from "react";

import {Conteudo,DivCard,DivInfo,DivGraph,DivInfoText,DivDep,DivInfoProf,keywords_carousel,map_carousel} from '../styles'
import { getImg, getPessoa, getKeywordsPessoa, getMapPessoa, getCountAllPessoa, getDashboardCount, getDashboardCountByDep} from '../api'
import { useHistory } from 'react-router-dom'
import Loading from '../components/loading'
import setBorderMenu from '../components/btnFunctions'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import KeywordsDocente from '../components/keywords-docente'
import MapDocente from '../components/map-docente'
import imageAvatar from '../assets/lattes_avatar_preto.png'
import LineChartDocente from '../components/chart-line-docente'

function ProfilePage() {

    function getMeta(url){   
        
        var img = new Image();
        img.addEventListener("load", function(){
            try{
            let img = document.querySelector('#image');
            img.style.backgroundAttachment = '';
            img.innerText = '';
            let color = [...document.querySelectorAll('a[id^=AU]')].filter(x=>x.style.backgroundColor)[0].style.backgroundColor;
            img.style.border = `8px solid ${color}`
            
            if((this.naturalWidth === this.naturalHeight) || ((this.naturalWidth < this.naturalHeight+10) && this.naturalWidth > this.naturalHeight-10)){
                img.style.backgroundPosition = 'center 0'
            }
            else if(this.naturalWidth > this.naturalHeight){
                img.style.backgroundSize = 'auto 145px '
                img.style.backgroundPosition = 'center 0'
            }
            if(this.naturalWidth < 200){
                img.style.backgroundPosition = 'center 0'
            }
            if(this.naturalWidth == 95 && this.naturalHeight == 99){
                        img.style.backgroundImage = `url(${imageAvatar})`
            }
            if(this.naturalWidth >750){
                img.style.backgroundSize = '140px auto'
    }
        } catch(e){
            return imageAvatar
        }
        });
        img.src = url;
        
        return img.src

    }
    let [user, setUser] = useState(null);
    const [dataUser, setDataUser] = useState(null);
    const history = useHistory()
    let {pathname} = history.location
    let idLattes = pathname.split('/')[3]
    useEffect( () => {
        (async function getData(){
        return await getImg(idLattes).then(u=> getMeta(u) ).then(u => setUser(u))
        })();
    }, []);
    useEffect( () => {
        (async function getDataUser(){
        return await getPessoa({id:idLattes}).then(u => setDataUser(u))
        })();
    }, []);

    if ( user == null || dataUser == null) {
      return <Conteudo style={{display: 'flex',justifyContent: 'center', alignItems: 'center', border: 'none'}}>
            {Loading()}
      </Conteudo>;
    }

    return (
        
      <>
        <Perfil foto = {user} dados = {dataUser}/>
      </>
    );
  }



function Perfil(props){
    
 
    setBorderMenu(props.dados.departamento)
    return(
        
        <Conteudo style={{justifyContent: 'start', width: 1100}}>
        <DivCard>
                <DivInfo>
                    <DivDep id="image" style={{fontSize: 12, backgroundImage: `url(${props.foto})`, backgroundAttachment: 'fixed'}}>
                    {Loading()}
                    </DivDep>
                    <DivInfoProf>
                        <span style={{fontSize: 25}}>{props.dados.nome_completo}</span>
                        <span>Telefone</span>

                        {props.dados.contato ? 
                        <span>+55 (11) {props.dados.contato}</span> :
                        <span>-</span>
                        }
                        <span>Currículo Lattes</span>
                        <a href={`http://lattes.cnpq.br/${props.dados.id_lattes}`} target="_blank">http://lattes.cnpq.br/{props.dados.id_lattes}</a>
                    </DivInfoProf>
                </DivInfo>

                <DivGraph>
                    <Carousel height="100%" showArrows={true} showThumbs={false} showIndicators={false} showStatus={false} autoPlay={true} infiniteLoop={true} interval={5000}>
                        <div style={keywords_carousel}>
                            <KeywordsDocente 
                                id={props.dados.id_lattes}
                                getKeywords={getKeywordsPessoa}
                                dep={props.dados.departamento}
                            />
                        </div>
                        <div style={map_carousel}>
                            <MapDocente
                                id={props.dados.id_lattes}
                                getMap={getMapPessoa}
                                dep={props.dados.departamento}
                            />
                        </div>  
                        <div style={map_carousel}>
                            <LineChartDocente
                                dep={props.dados.departamento}
                                id={props.dados.id_lattes}
                                getDashboardCount={getDashboardCountByDep}
                                getCountDocente={getCountAllPessoa}
                            />
                        </div>                                                  
                    </Carousel>
                </DivGraph>  
                <DivInfoText>
                    <span style={{fontSize: 25, fontWeight: "bold", marginTop: -3.8}} >Resumo Acadêmico</span>
                    <span>{props.dados.resumo}</span>
                </DivInfoText>
                </DivCard>
        </Conteudo>
        
    )
}

export default ProfilePage



