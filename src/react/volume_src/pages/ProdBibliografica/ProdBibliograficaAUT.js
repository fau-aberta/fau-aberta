import React from 'react'
import ChartsDepartment from '../../components/charts-department'
import { getProdBibCount, getProdBibTipos, getProdBibRank } from '../../api'

function ProdBibliograficaAUT() {
    return (
        <ChartsDepartment
            title="Produção Bibliográfica"
            department="AUT"
            getCount={getProdBibCount}
            getTipos={getProdBibTipos}
            getRank={getProdBibRank}
        />
    )
}

export default ProdBibliograficaAUT