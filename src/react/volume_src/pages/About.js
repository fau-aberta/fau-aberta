import React from 'react'
import styled from 'styled-components'
import { CenteredColumn } from '../styles'
import LogoUSP from '../assets/logo-usp.png'
import FAU from '../assets/fau.png'
import IME from '../assets/ime.png'
import CNPQ from '../assets/cnpq.png'
import STI from '../assets/sti.png'
import FotoTime from '../assets/time.jpeg'

function About() {
    return (
        <CenteredColumn>
            <Title>FAU Aberta</Title>

            <CenteredColumn>
                <h2>Um projeto</h2>
                <Label>Universidade de São Paulo</Label>
                <Image src={LogoUSP} style={{width: 280, height: 114}} />
                <span>Reitor Vahan Agopyan</span>
                <span>Vice-Reitor Antonio Carlos Hernandes</span>
                <Image src={FotoTime} style={{width: 600, height: 400}} />
                <span>Foto da equipe</span>
            </CenteredColumn>

            <Row>
                <CenteredColumn>
                    <h2>Idealização</h2>
                    <Label>Faculdade de Arquitetura e Urbanismo</Label>
                    <Image src={FAU} style={{width: 280, height: 114}} />
                    <span>Diretora Ana Lúcia Duarte Lanna</span>
                    <span>Vice-Diretor Eugenio Fernandes Queiroga</span>
                    <Label>Comissão de Pesquisa da FAU-USP</Label>
                    <label>Presidente Beatriz Piccolotto Siqueira Bueno</label>
                    <label>Vice-Presidente Marta Vieira Bogéa</label>
                    <label>Consultoria em design gráfico Leandro Velloso</label>
                    <Label>Serviço Técnico de Biblioteca da FAU-USP</Label>
                    <label>Chefe Técnica Mônica de Arruda Nascimento</label>
                    <label>Bibliotecária Amarílis Montagnolli Gomes Corrêa</label>
                    <MiniLabel>Seção Técnica de Informática da FAU-USP</MiniLabel>
                    <span>Chefe Técnico Harley Macedo</span>
                    <span>Técnico em redes Deidson Rafael da Silva Trindade</span>
                </CenteredColumn>

                <CenteredColumn>
                    <h2>Realização</h2>
                    <Label>Instituto de Matemática e Estatística</Label>
                    <Image src={IME} style={{width: 140, height: 170}} />
                    <span>Diretor Junior Barrera</span>
                    <span>Vice-Diretor Luiz Renato Gonçalves Fontes</span>
                    <Label>Disciplina Laboratório de Programação Extrema</Label>
                    <MiniLabel style={{marginTop: 0}}>Responsáveis</MiniLabel>
                    <span>Responsável Alfredo Goldman Vel Lejbman</span>
                    <span>PAE voluntária Thatiane de Oliveira Rosa</span>
                    <MiniLabel>Alunos</MiniLabel>
                    <span>César Gasparini Fernandes</span>
                    <span>Leonardo de Carvalho Freitas Padilha Aguilar</span>
                    <span>Larissa Goto Sala</span>
                    <span>Mateus Agostinho dos Anjos</span>
                    <span>Matheus Lima Cunha</span>
                    <span>Nathalia Orlandi Borin</span>
                    <span>Pedro Vítor Bortolli Santos</span>
                    <span>Victor André Batistella</span>
                </CenteredColumn>
            </Row>

            <Row style={{marginBottom: 32}}>
                <CenteredColumn>
                    <h2>Suporte</h2>
                    <Label>Superintendência de Tecnologia da Informação da USP</Label>
                    <Image src={STI} style={{width: 280, height: 90}} />
                    <span>Superintendente: Prof. Dr. João Eduardo Ferreira</span>
                </CenteredColumn>

                <CenteredColumn>
                    <h2>Fornecimento de dados</h2>
                    <Label>Conselho Nacional de Desenvolvimento Científico e Tecnológico</Label>
                    <Image src={CNPQ} style={{width: 280, height: 100}} />
                </CenteredColumn>
            </Row>
        </CenteredColumn>
    )
}

const Title = styled.span`
    margin: 32px 0px 16px;
    font-size: 48px;
`
const Label = styled.span`
    margin: 16px 0px 16px;
    font-size: 22px;
`
const Image = styled.img`
    margin: 24px 0px 24px;
`
const Row = styled.div`
    display: flex;
    justify-content: center;
    > :not(:last-child) {
        margin-right: 120px;
    }
    margin-top: 24px;
`
const MiniLabel = styled.span`
    font-size: 20px;
    margin: 8px 0px 8px;
`

export default About