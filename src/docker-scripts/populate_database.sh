#!/bin/bash

#Popula o banco de dados usando o parser.

help(){
  echo "Popula o banco de dados usando o parser.py e os xmls do lattes."
  echo "Uso: populate_database"
  echo -e "-h/--help \t Mostra instruções de uso."
  echo -e "-v/--verbose \t Ativa o modo verborrágico do parser."
  echo -e "-nup/--no-up \t Não realiza o docker-compose up/down."
  exit
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
VERB=""
UP="T"

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -nup|--no-up)
      UP="F"
      ;;

    -v|--verbose)
      VERB=" -v"
      ;;

    -?*)
      echo "Comando não reconhecido: $1."
      help
      ;;

    *)
      break
      
  esac
  shift
done

if [ "$VERB" == "" ];then
  while :; do
    read -p "Ativar modo verborrágico? (y/n) " -n 1 -r
    echo
    case $REPLY in
      Y|y|S|s)
        VERB=" -v"
        break
        ;;

      N|n)
        break
        ;;

      *)
        echo "Resposta inválida."

    esac
  done
fi

if [ "$UP" == "T" ];then
  cd "$DIR"/
  ./docker-up.sh db web
fi

cd "$DIR"/..
docker exec -it fauserver_django bash -c "cd populate/code; python3 parser.py$VERB"

if [ "$UP" == "T" ];then
  cd "$DIR"/
  ./docker-up.sh -d
fi