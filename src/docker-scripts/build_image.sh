#!/bin/bash

#Refaz as imagens do docker sem usar o cache (a partir do zero)
#Alternativamente, pode-se informar uma imagem especifica

help(){
  echo "Refaz as imagens do docker."
  echo "Uso: build_image [ARGS]"
  echo -e "-h/--help \t\t\t Mostra instruções de uso."
  echo -e "-nc/--no-cache \t\t\t Monta as imagens sem usar cache (a partir do zero)."
  echo -e "-i/--imagem <imagem> \t\t Monta somente a imagem <imagem>."
  exit
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ARGS=""

cd "$DIR"/..

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -nc|--no-cache)
      ARGS=" --no-cache$ARGS"
      ;;

    -i|--imagem)
      ARGS="$ARGS $2"
      shift
      ;;

    -?*)
      echo "Comando não reconhecido $1."
      help
      ;;

    *)
      break
      
  esac
  shift
done


docker-compose build${ARGS}