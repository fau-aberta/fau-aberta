#!/bin/bash

#Realiza os teste do django.

help(){
  echo "Realiza os teste do django."
  echo "Uso: test_django"
  echo -e "-h/--help \t Mostra instruções de uso."
  echo -e "-nup/--no-up \t Não realiza o docker-compose up/down."
  echo -e "-t/--teste <app> \t Roda somente os testes para <app>"
  exit
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
UP="T"

TESTE=""

while :; do
  case $1 in
    -h|--help)
      help
      ;;

    -nup|--no-up)
      UP="F"
      ;;

    -?*)
      echo "Comando não reconhecido: $1."
      help
      ;;
    
    --teste|-t)
      TESTE=" $2"
      shift
      ;;
    
    *)
      break

  esac
  shift
done

if [ "$UP" == "T" ];then
  cd "$DIR"/
  ./docker-up.sh db web
fi

docker exec fauserver_django python3 manage.py test"$TESTE"

RET=$?

if [ "$UP" == "T" ];then
  ./docker-up.sh -d
fi

exit $RET