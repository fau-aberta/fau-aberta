var host = process.env.HOST || '0.0.0.0';
var port = 8001;

var cors_proxy = require('cors-anywhere');
cors_proxy.createServer({
    originWHitelist: [],
    requrieHeader: ['origin', 'x-requested-with'],
    removeHeaders: ['cookie', 'cookie2']
}).listen(port, host, function() {
    console.log('Running CORS Anywhere on ' + host + ':' + port);
})